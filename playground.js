const AES = require('crypto-js/aes');

const text = 'Some Advanced Text To Show';

console.time('encryption');
const encrypted = AES.encrypt(text, 'encryption_key');
console.timeEnd('encryption');
console.log({
    text,
    encrypted: encrypted.toString(),
});

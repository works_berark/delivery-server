const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

// Router Validations

validation.rateOrder = Joi.object().keys({
    order: Joi.object()
        .keys({
            id: validationUtils.idValidation.required(),
            rating: validationUtils.ratingValidation,
            message: Joi.string().trim(),
        })
        .required(),
    partner: Joi.object().keys({
        id: validationUtils.idValidation.required(),
        rating: validationUtils.ratingValidation,
        message: Joi.string().trim(),
    }),
    products: Joi.array().items({
        productId: validationUtils.idValidation,
        images: Joi.array().items(validationUtils.urlValidation),
        rating: validationUtils.ratingValidation,
        message: Joi.string().trim(),
    }),
});

validation.getProductRatings = Joi.object().keys({
    productId: validationUtils.idValidation.required(),
});

validation.ratePartner = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
    partnerId: validationUtils.idValidation.required(),
    rating: validationUtils.ratingValidation,
    images: Joi.array().items(validationUtils.urlValidation),
    message: Joi.string().trim(),
});

validation.rateUser = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
    userId: validationUtils.idValidation.required(),
    rating: validationUtils.ratingValidation,
    images: Joi.array().items(validationUtils.urlValidation),
    message: Joi.string().trim(),
});

validation.removeImage = Joi.object().keys({
    image: validationUtils.urlValidation.required(),
});

module.exports = validation;

const Joi = require('joi');

const validationUtils = require('../utils/validation.utils');

const validation = {};

// Router Validations
validation.updateWalkThrough = Joi.object().keys({
    pages: Joi.array()
        .items(
            Joi.object().keys({
                title: Joi.string().required(),
                description: Joi.string().required(),
                image: validationUtils.urlValidation.required(),
                hexColor: Joi.string().required(),
                translations: Joi.array()
                    .items(
                        Joi.object().keys({
                            languageCode: Joi.string().required(),
                            languageId: validationUtils.idValidation,
                            title: Joi.string().required(),
                            description: Joi.string().required(),
                        })
                    )
                    .required(),
            })
        )
        .required(),
});

validation.updateHomeScreen = Joi.object().keys({
    homeContentType: Joi.string().valid('SIGN_IN', 'SIGN_OUT').required(),
    homeContents: Joi.array()
        .items(
            Joi.object().keys({
                title: validationUtils.nameValidation,
                type: Joi.string()
                    .valid('CATEGORIES', 'BEST_SELLER', 'MUST_TRY')
                    .required(),
                image: validationUtils.urlValidation.default(''),
                style: Joi.string()
                    .valid(
                        'SINGLE_BANNER',
                        'MULTIPLE_BANNER',
                        'SMALL_SQUARE',
                        'BIG_RECTANGLE',
                        'SMALL_RECTANGLE'
                    )
                    .required(),
                scroll: Joi.string().valid('HORIZONTAL', 'VERTICAL').required(),
                translations: Joi.array()
                    .items(
                        Joi.object().keys({
                            languageCode: Joi.string().required(),
                            languageId: validationUtils.idValidation,
                            title: Joi.string().required(),
                        })
                    )
                    .required(),
            })
        )
        .required(),
});

validation.getCityAndHomeScreen = Joi.object().keys({
    lat: Joi.number().required(),
    lng: Joi.number().required(),
});

module.exports = validation;

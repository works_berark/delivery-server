const express = require('express');
const router = express.Router();

const multer = require('multer');
const upload = multer();

const controller = require('../controllers/shops.controller');
const { adminTokenVerify, userTokenVerify, shopTokenVerify } = require('../utils/auth.utils');
const { validateBody } = require('../utils/errors.utils');
const validations = require('../validations/shops.validation');
const { paginationParams } = require('../utils/helper.utils');

// File Uploads
const addCategoryUpload = upload.fields([{ name: 'image' }, { name: 'secondaryImage' }]);
const addSubCategoryUpload = upload.fields([{ name: 'image' }, { name: 'secondaryImage' }]);
const addProductsUpload = upload.fields([{ name: 'image' }, { name: 'secondaryImage' }]);

// --------- ADMIN PANEL -------------------
router.post('/getAllShops', adminTokenVerify, paginationParams, validateBody(validations.getAllShops), controller.getAllShops);
router.post('/addShop', adminTokenVerify, validateBody(validations.addShop), controller.addShop);
router.post('/getShop', adminTokenVerify, controller.getShop);
router.post('/shopOnlineStatusChange', adminTokenVerify, controller.shopOnlineStatusChange);
router.post('/uploadImage', adminTokenVerify, upload.single('avatar'), controller.uploadImage);

// Heads
router.post('/getAllHeads', adminTokenVerify, paginationParams, validateBody(validations.getAllHeads), controller.getAllHeads);
router.post('/getHead', adminTokenVerify, controller.getHead);
router.post('/convertShopToHead', adminTokenVerify, upload.single('avatar'), validateBody(validations.convertShopToHead), controller.convertShopToHead);
router.post('/addShopToHead', adminTokenVerify, validateBody(validations.addShopToHead), controller.addShopToHead);
router.post('/getHeadsNames', adminTokenVerify, controller.getHeadsNames);
router.post('/getHeadsCatalogues', adminTokenVerify, validateBody(validations.getHeadsCatalogues), controller.getHeadsCatalogues);

// Catalogues
router.post('/getAllCatalogueNames', adminTokenVerify, controller.getAllCatalogueNames);
router.post('/getAllCatalogues', adminTokenVerify, paginationParams, controller.getAllCatalogues);
router.post('/getCatalogue', adminTokenVerify, controller.getCatalogue);
router.post('/getCatalogueContents', adminTokenVerify, controller.getCatalogueContents);

router.post('/addCatalogue', adminTokenVerify, validateBody(validations.addCatalogue), controller.addCatalogue);
router.post('/addCategory', adminTokenVerify, addCategoryUpload, validateBody(validations.addCategory), controller.addCategory);
router.post('/addSubCategory', adminTokenVerify, addSubCategoryUpload, validateBody(validations.addSubcategory), controller.addSubcategories);
router.post('/addProducts', adminTokenVerify, addProductsUpload, validateBody(validations.addProducts), controller.addProducts);

router.post('/getCategories', adminTokenVerify, controller.getCategories);
router.post('/getSubCategories', adminTokenVerify, controller.getSubCategories);
router.post('/getProducts', adminTokenVerify, controller.getProducts);

router.post('/deleteCategory', adminTokenVerify, controller.deleteCategory);
router.post('/deleteSubCategory', adminTokenVerify, controller.deleteSubCategory);
router.post('/deleteProduct', adminTokenVerify, controller.deleteProduct);

router.post('/toggleStatusCategory', adminTokenVerify, validateBody(validations.statusValidation), controller.toggleStatusCategory);
router.post('/toggleStatusSubCategory', adminTokenVerify, validateBody(validations.statusValidation), controller.toggleStatusSubCategory);
router.post('/toggleStatusProduct', adminTokenVerify, validateBody(validations.statusValidation), controller.toggleStatusProduct);

// --------- SHOP PANEL --------------------
router.post('/loginWithEmail', validateBody(validations.loginWithEmail), controller.loginWithEmail);
router.post('/loginValidation', shopTokenVerify, controller.loginValidation);
router.post('/getDashboardData', shopTokenVerify, controller.getDashboardData);

// Partners
router.post('/getPartnersLists', shopTokenVerify, paginationParams, controller.getPartnersLists);
router.post('/getPartnerView', shopTokenVerify, controller.getPartnerView);

// --------- MOBILE -------------------
router.post('/getSingleShopDetails', validateBody(validations.getSingleShopDetails), controller.getSingleShopDetails);
router.post('/getSingleShopSearch', validateBody(validations.getSingleShopSearch), controller.getSingleShopSearch);

module.exports = router;

const express = require('express');
const router = express.Router();

const controller = require('../controllers/languages.controller');
const validation = require('../validations/languages.validation');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

router.post('/getAllLanguages', adminTokenVerify, paginationParams, controller.getAllLanguages);
router.post('/getAllLanguageNames', adminTokenVerify, controller.getAllLanguageNames);
router.post('/addLanguage', adminTokenVerify, validateBody(validation.addLanguage), controller.addLanguage);
router.post('/getLanguage', adminTokenVerify, validateBody(validation.getLanguage), controller.getLanguage);
router.post('/getViewLanguage', adminTokenVerify, validateBody(validation.getViewLanguage), controller.getViewLanguage);
router.post('/setDefaultLanguage', adminTokenVerify, validateBody(validation.setDefaultLanguage), controller.setDefaultLanguage);
router.post('/bingTranslateAdminKeys', adminTokenVerify, validateBody(validation.bingTranslateAdminKeys), controller.bingTranslateAdminKeys);

// Mobile Update language Mobile Keys
router.post('/updateMobileKeys', adminTokenVerify, validateBody(validation.updateMobileKeys), controller.updateMobileKeys);
router.get('/getMobileUserLangKeys/:langCode', controller.getMobileUserLangKeys);
router.get('/getMobilePartnerLangKeys/:langCode', controller.getMobilePartnerLangKeys);

module.exports = router;

const utils = {};

const mail = require('./mail.utils');

utils.send = ({ to = '', body = '', from = '' }) =>
    new Promise((resolve, reject) => {
        const SSID = 'AC296120ddcebbcf95d0321fe9d4a7395f';
        const authToken = '1422c6f962b5f5542a59b80dace09065';
        // const accountSid = process.env.TWILIO_ACCOUNT_SID;
        // const authToken = process.env.TWILIO_AUTH_TOKEN;
        const client = require('twilio')(SSID, authToken);

        client.messages
            .create({
                body,
                from: '+12183775925',
                to,
            })
            .then((data) => {
                // console.log(data);
                resolve(true);
            })
            .catch((err) => {
                console.log(err);
                resolve(true);
                mail.sendMail(
                    'benzigar619@gmail.com',
                    'Twilio Eror',
                    JSON.stringify(err.message)
                );
            });
    });

module.exports = utils;

const chalk = require('chalk');

const helperUtils = require('../utils/helper.utils');

const utils = {};

utils.validateSocket = (schema, ns, event, body, socket, partner) => {
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true, // remove unknown props
    };
    const { error, value } = schema.validate(body, options);
    if (error) {
        console.log(chalk.yellow(JSON.stringify(body, null, 2)));
        console.log(
            JSON.stringify(
                error.details.map((each) => each.message),
                null,
                2
            )
        );
        helperUtils.validationLogging(
            ns + ' ' + event,
            'SOCKET',
            body,
            error?.details?.map((each) => each?.message)
        );
        return false;
    } else {
        return value;
    }
};

utils.validateBody = (schema) => (req, res, next) => {
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true, // remove unknown props
    };
    const { error, value } = schema.validate(req.body, options);
    if (error) {
        console.log(chalk.yellow(JSON.stringify(req.body, null, 2)));
        console.log(
            JSON.stringify(
                error.details.map((each) => each.message),
                null,
                2
            )
        );
        helperUtils.validationLogging(
            req.baseUrl + req.path,
            'API',
            req?.body,
            error.details.map((each) => each.message)
        );
        res.status(422).json({
            code: 422,
            message: error.details.map((each) => each.message),
        });
    } else {
        req.body = value;
        next();
    }
};

utils.handlerError = async (err, req, res) => {
    // Draw Line for Serious Error
    if (err.message)
        console.log(chalk.redBright('-'.repeat(process.stdout.columns)));
    // If User Info Available
    if (err) {
        if (req?.user?.data?.phone?.number)
            console.log(
                chalk.cyan(
                    req.user.data.phone.code + ' ' + req.user.data.phone.number
                )
            );
    }
    // Console Error
    if (err.message) {
        if (Object.keys(req.body).length > 0)
            console.log(JSON.stringify(req.body, null, 2));
        console.log(
            chalk.redBright(err.stack) + '\t' + chalk.redBright(req.path)
        );
        helperUtils.errorLogging(req, err);
        res.status(500).json({ code: 500, message: err.message });
    } else {
        console.log(
            chalk.yellow(err.includes('|') ? err.split('|')[1] : err) +
                '\t' +
                chalk.yellow(req.path)
        );
        helperUtils.clientErrorLogging(
            req.baseUrl + req.path,
            err.includes('|') ? err.split('|')[0] : 500,
            req?.body,
            err.includes('|') ? err.split('|')[1] : err,
            {
                id: req?.user?._id,
                phoneCode: req?.user?.phone?.code,
                phoneNumber: req?.user?.phone?.number,
            }
        );
        res.status(err.includes('|') ? err.split('|')[0] : 500).json({
            code: err.includes('|') ? parseInt(err.split('|')[0]) : 500,
            langKey: err.includes('|') ? err.split('|')[1] : err,
            message: await helperUtils.getLanguageKey(
                err.includes('|') ? err.split('|')[1] : err,
                req?.headers['language-code'] || 'en'
            ),
        });
    }
    // Response Error
    // if (err.message) {
    // } else {
    // }
};

module.exports = utils;

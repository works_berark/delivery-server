const { v4: uuid } = require('uuid');
// const puppeteer = require('puppeteer');
const jsdom = require('jsdom');

const { JSDOM } = jsdom;

const Other = require('../models/other.model');
const Language = require('../models/language.model');
const languages = require('../json/languages.json');

const helperUtils = require('./helper.utils');
const mailUtils = require('./mail.utils');

const jobs = {};

jobs.activeJobLists = [];

jobs.getActiveJobLists = () =>
    jobs.activeJobLists.sort((a, b) =>
        a.date > b.date ? -1 : a.date < b.date
    );

jobs.getActiveLanguageTranslateJobs = () =>
    jobs.activeJobLists
        .filter((each) => each.action === 'LANGUAGE_BING_SCRAPE')
        .sort((a, b) => (a.date > b.date ? -1 : a.date < b.date));

jobs.translateFromBingScrapping = (obj = {}, target = 'en', languageId = '') =>
    new Promise(async (res, rej) => {
        let converted = {};
        const id = uuid();
        try {
            const date = new Date();

            const lang = languages.filter((each) => each.code === target);

            if (lang.length > 0) {
                const config = await Other.config();
                const selector =
                    config?.scrapping?.bingTranslateSelector ??
                    '#tta_output_ta';
                jobs.activeJobLists.push({
                    id,
                    action: 'LANGUAGE_BING_SCRAPE',
                    process: 0,
                    targetLanguage: target,
                    date,
                });
                const languageName = lang[0].name;
                for await (const [index, each] of Object.keys(obj).entries()) {
                    let content;
                    let retry = 1;
                    const url = `https://www.bing.com/search?q=translate+${encodeURIComponent(
                        obj[each]
                    )}+to+${languageName}`;
                    while (true) {
                        const dom = await JSDOM.fromURL(url).catch((err) =>
                            console.log(err)
                        );
                        // console.log(dom.serialize());
                        // await helperUtils.sleep(800);
                        // return;
                        content = dom.window.document.querySelector(selector)
                            ? dom.window.document.querySelector(selector).value
                            : '';
                        if (content !== '...' || retry === 5) break;
                        retry++;
                    }
                    converted[each] =
                        content && content !== '...' ? content : obj[each];
                    const percentage = parseFloat(
                        ((index + 1) / Object.keys(obj).length) * 100
                    ).toFixed(1);
                    jobs.activeJobLists = jobs.activeJobLists.filter(
                        (each) => each.id !== id
                    );
                    jobs.activeJobLists.push({
                        id,
                        action: 'LANGUAGE_BING_SCRAPE',
                        process: parseFloat(percentage),
                        targetLanguage: target,
                        date,
                    });
                }
                await Language.findByIdAndUpdate(languageId, {
                    $set: {
                        languageAdminKeys: converted,
                    },
                });
                jobs.activeJobLists = jobs.activeJobLists.filter(
                    (each) => each.id !== id
                );
            } else {
                converted = obj;
            }
            res(converted);
        } catch (err) {
            // mailUtils.sendMail(
            //     'benzigar@berarkrays.com',
            //     'Code Error !!! ',
            //     `
            //     <p>${err.stack}</p>
            //     <p>${new Date()}</p>
            // `
            // );
            console.log(err.stack);
            jobs.activeJobLists = jobs.activeJobLists.filter(
                (each) => each.id !== id
            );
            res(converted);
        }
    });

// jobs.translateFromGoogleScrappingPrettier = (obj = {}, target = 'en') =>
//     new Promise(async (res, rej) => {
//         const id = uuid();
//         const date = new Date();
//         jobs.activeJobLists.push({
//             id,
//             action: 'LANGUAGE_GOOGLE_SCRAPE',
//             process: 0,
//             targetLanguage: target,
//             date,
//         });
//         const translatedData = {};
//         const browser = await puppeteer.launch({
//             headless: true, // false: enables one to view the Chrome instance in action
//             executablePath: '/snap/bin/chromium',
//             args: [
//                 '--no-sandbox',
//                 '--disable-setuid-sandbox',
//                 '--disable-dev-shm-usage',
//                 '--disable-accelerated-2d-canvas',
//                 '--no-first-run',
//                 '--no-zygote',
//                 '--single-process', // <- this one doesn't works in Windows
//                 '--disable-gpu',
//             ],
//         });
//         const url = `https://translate.google.co.in/?sl=en&tl=${target}&text=start&op=translate`;
//         const page = await browser.newPage();
//         console.log('Passed through !! ');
//         await page.goto(url, { waitUntil: 'load', timeout: 0 });
//         console.log('page loaded');
//         await page.waitForSelector('span[jsname="W297wb"]', { visible: true });
//         console.log('----- Translation Started ------ ');
//         for await (const [index, each] of Object.keys(obj).entries()) {
//             const input = await page.$('textarea[aria-label="Source text"]');
//             await input.click({ clickCount: 3 });
//             await input.type(obj[each]);
//             await helperUtils.sleep(1200);
//             translatedData[each] = await page.evaluate(() => {
//                 return document.querySelector('span[jsname="W297wb"]')
//                     .innerText;
//             });
//             const percentage = parseFloat(
//                 ((index + 1) / Object.keys(obj).length) * 100
//             ).toFixed(1);
//             jobs.activeJobLists = jobs.activeJobLists.filter(
//                 (each) => each.id !== id
//             );
//             jobs.activeJobLists.push({
//                 id,
//                 action: 'LANGUAGE_GOOGLE_SCRAPE',
//                 process: parseFloat(percentage),
//                 targetLanguage: target,
//                 date,
//             });
//         }
//         await browser.close();
//         jobs.activeJobLists = jobs.activeJobLists.filter(
//             (each) => each.id !== id
//         );
//         res(translatedData);
//     });

module.exports = jobs;

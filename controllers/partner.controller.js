const { subMinutes } = require('date-fns');
const handler = require('express-async-handler');
const { v4: uuid } = require('uuid');
const { distance: turfDistance, point: turfPoint } = require('@turf/turf');
const differenceInMinutes = require('date-fns/differenceInMinutes');

const User = require('../models/user.model');
const Other = require('../models/other.model');
const Order = require('../models/order.model');
const Activity = require('../models/activity.model');
const Language = require('../models/language.model.js');
const Partner = require('../models/partner.model');
const Vehicle = require('../models/vehicle.model');
const Zone = require('../models/zone.model');
const Product = require('../models/product.model');
const Notification = require('../models/notification.model');
const Rating = require('../models/rating.model');

const auth = require('../utils/auth.utils');
const redis = require('../utils/redis.utils');
const smsUtils = require('../utils/sms.utils');
const helperUtils = require('../utils/helper.utils');
const mail = require('../utils/mail.utils');
const fileUtils = require('../utils/upload.utils');
const fcm = require('../utils/fcm.utils');

const constantUtils = require('../utils/constant.utils');
const structureUtils = require('../utils/structure.utils');

const controller = {};

controller.getAllPartners = handler(async (req, res) => {
    const partnerConnections = await redis.get(
        constantUtils.PARTNER_CONNECTIONS
    );
    let condition = {
        $or: helperUtils.generateSearch(
            ['firstName', 'lastName', 'email', 'phone.number', 'phone.code'],
            req.body.search
        ),
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.filters?.status)
        condition['status'] = req.body.filters.status;
    if (req?.body?.filters?.gender)
        condition['gender'] = req.body.filters.gender;
    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };
    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };
    if (req?.body?.filters?.reFillRequest)
        condition['reFillRequest.status'] = req?.body?.filters?.reFillRequest;

    const count = await Partner.count(condition);
    const partners = await Partner.find(condition)
        .select(
            'firstName lastName partnerType email phone avatar createdAt onGoingOrders rating status onlineStatus deviceInfo reFillRequest'
        )
        .sort(sort)
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .lean();

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW_LIST,
        listType: constantUtils.PARTNERS,
    });

    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: partners.map((each) => ({
            id: each?._id,
            partnerType: each?.partnerType,
            firstName: each?.firstName,
            lastName: each?.lastName,
            email: each?.email,
            avatar: each?.avatar,
            joined: each?.createdAt,
            phone: each?.phone,
            onlineStatus: each?.onlineStatus,
            reFillRequest: each?.reFillRequest,
            onGoingOrders: each?.onGoingOrders,
            status: each?.status,
            rating: each?.rating,
            socketConnected: each?.deviceInfo?.[0]?.deliverySocketId
                ? partnerConnections.includes(
                      each?.deviceInfo?.[0]?.deliverySocketId
                  )
                : false,
        })),
    });
});

controller.addPartner = handler(async (req, res) => {
    const partnerUpdateData = {
        cityId: req?.body?.cityId,
        zones: [
            {
                zoneId: req?.body?.zoneId,
                zoneType: 'PRIMARY',
            },
        ],
        firstName: req?.body?.firstName,
        lastName: req?.body?.lastName,
        email: req?.body?.email,
        'phone.code': req?.body?.phoneCode,
        'phone.number': req?.body?.phoneNumber,
        gender: req?.body?.gender,
        dob: req?.body?.dob,
        avatar: req?.body?.avatar,
        partnerType: req?.body?.partnerType,
        partnerDocuments: req?.body?.partnerDocuments,
        languageCode: req?.body?.languageCode,
        catalogueId: req?.body?.catalogueId,
        lastEdited: req?.user?._id,
        lastEditedTime: new Date(),
    };

    const vehicleUpdateData = {
        name: req?.body?.vehicle?.name,
        plateNumber: req?.body?.vehicle?.plateNumber,
        maker: req?.body?.vehicle?.maker,
        model: req?.body?.vehicle?.model,
        year: req?.body?.vehicle?.year,
        color: req?.body?.vehicle?.color,
        noOfDoors: req?.body?.vehicle?.noOfDoors,
        noOfSeats: req?.body?.vehicle?.noOfSeats,
        frontImage: req?.body?.vehicle?.frontImage,
        backImage: req?.body?.vehicle?.backImage,
        leftImage: req?.body?.vehicle?.leftImage,
        rightImage: req?.body?.vehicle?.rightImage,
        vehicleDocuments: req?.body?.vehicle?.vehicleDocuments,
        lastEdited: req?.user?._id,
        lastEditedTime: new Date(),
    };

    if (req?.body?.partnerId) {
        const ifPartner = await Partner.count({
            _id: { $ne: req?.body?.partnerId },
            $or: [
                {
                    email: req?.body?.email,
                },
                {
                    'phone.number': req?.body?.phoneNumber,
                },
            ],
        });
        if (ifPartner > 0)
            throw '400|' + constantUtils.EMAIL_OR_PHONE_NUMBER_ALREADY_EXISTS;

        let partner = await Partner.findByIdAndUpdate(
            req?.body?.partnerId,
            {
                $set: {
                    ...partnerUpdateData,
                },
            },
            {
                new: true,
            }
        );
        // if (partner?.catalogueId !== req?.body?.catalogueId) {
        //     console.log(partner?.catalogue);
        //     const products = await Product.find({
        //         catalogueId: req?.body?.catalogueId,
        //     });
        //     const obj = products.map((each) => ({
        //         productId: each?._id,
        //         stockQuantity:
        //             each?.productType === constantUtils.QUANTITY_STOCK
        //                 ? 0
        //                 : undefined,
        //         status: constantUtils.ACTIVE,
        //     }));
        //     partner = await Partner.findByIdAndUpdate(
        //         req?.body?.partnerId,
        //         {
        //             $set: {
        //                 availableProducts: obj,
        //             },
        //         },
        //         {
        //             new: true,
        //         }
        //     );
        // }
        let vehicle = {};
        if (req?.body?.vehicle?.vehicleId) {
            vehicle = await Vehicle.findByIdAndUpdate(
                req?.body?.vehicle?.vehicleId,
                {
                    $set: {
                        ...vehicleUpdateData,
                    },
                },
                {
                    new: true,
                }
            );
        }
        return res.json({
            partner,
            vehicle,
        });
    } else {
        if (req?.body?.catalogueId) {
            const products = await Product.find({
                catalogueId: req?.body?.catalogueId,
            });
            const obj = products.map((each) => ({
                productId: each?._id,
                stockQuantity:
                    each?.productType === constantUtils.QUANTITY_STOCK
                        ? 0
                        : undefined,
                maxStockQuantity:
                    each?.productType === constantUtils.QUANTITY_STOCK
                        ? 0
                        : undefined,
                status: constantUtils.ACTIVE,
            }));
            partnerUpdateData['availableProducts'] = obj;
        }
        const ifPartner = await Partner.count({
            $or: [
                {
                    email: req?.body?.email,
                },
                {
                    'phone.number': req?.body?.phoneNumber,
                },
            ],
        });
        if (ifPartner > 0)
            throw '400|' + constantUtils.EMAIL_OR_PHONE_NUMBER_ALREADY_EXISTS;

        let partner = await Partner.create({
            ...partnerUpdateData,
            password: req?.body?.password
                ? auth.generatePassword(req?.body?.password)
                : '',
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });

        const vehicle = await Vehicle.create({
            ...vehicleUpdateData,
            ownerId: partner._id,
            assignedId: partner._id,
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });

        partner.vehicles = [
            {
                vehicleId: vehicle._id,
                defaultVehicle: true,
            },
        ];

        await partner.save();

        return res.json({ partner, vehicle });
    }
});

controller.getViewPartner = handler(async (req, res) => {
    const partnerConnections = await redis.get(
        constantUtils.PARTNER_CONNECTIONS
    );
    const [recentRatings, partner] = await Promise.all([
        Rating.find({
            ratingFor: constantUtils.PARTNER,
            partnerId: req?.body?.id,
        })
            .sort('-_id')
            .limit(5)
            .populate('userId', 'firstName lastName email avatar'),
        Partner.findOne({
            _id: req?.body?.id,
        })
            .populate(
                'addedBy',
                'data.avatar _id data.firstName data.lastName data.email data.status'
            )
            .populate('onGoingOrders.orderId')
            .populate(
                'lastEdited',
                'data.avatar _id data.firstName data.lastName data.email data.status'
            )
            .populate('zones.zoneId', '_id zoneName location')
            .populate(
                'cityId',
                '_id location currencyCode currencySymbol locationName'
            )
            .populate('catalogueId')
            .populate('reFillRequest.attendedBy')
            .populate('vehicles.vehicleId')
            .lean(),
    ]);
    return res.json({
        ...partner,
        recentRatings: recentRatings,
        connection: partnerConnections.includes(
            partner?.deviceInfo?.[0]?.deliverySocketId
        ),
    });
});

controller.emailPasswordLogin = handler(async (req, res) => {
    const ifPartner = await Partner.findOne({
        email: req.body.email,
    }).select('password status');
    if (ifPartner && ifPartner.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (!ifPartner || !ifPartner.password)
        throw '404|' + constantUtils.INVALID_CREDENTIALS;
    else {
        if (auth.comparePassword(req.body.password, ifPartner.password)) {
            const token = auth.jwtSign({
                _id: ifPartner._id,
                type: constantUtils.PARTNER,
                deviceType: req?.body?.platform,
                platform: req?.body?.platform + '/' + req?.useragent?.source,
            });
            const loggedInPartner = await Partner.findByIdAndUpdate(
                ifPartner._id,
                {
                    $set: {
                        deviceInfo: [
                            {
                                accessToken: token,
                                deviceId: req?.body?.deviceId,
                                deviceType: req?.body?.platform,
                                platform:
                                    req?.body?.platform +
                                    '/' +
                                    req?.useragent?.source,
                            },
                        ],
                    },
                },
                { new: true }
            )
                .populate('zones.zoneId', 'zoneName location')
                .populate(
                    'cityId',
                    'locationName timezone currencySymbol currencyCode _id orderCalculation'
                )
                .populate('vehicles.vehicleId');
            // Activity.create({
            //     fromId: logedInUser._id,
            //     fromType: constantUtils.ACTIVITIES.USERS,
            //     action: constantUtils.ACTIVITIES.LOG_IN,
            // });
            return res.json(
                structureUtils.partnerLoginStructure(loggedInPartner)
            );
        } else throw '404|' + constantUtils.INVALID_CREDENTIALS;
    }
});

controller.checkLogin = handler(async (req, res) => {
    return res.json(req?.user ?? {});
});

controller.getProfile = handler(async (req, res) => {
    const token = req?.headers?.authorization?.split(' ')?.[1];
    await Partner.updateOne(
        {
            _id: req?.user?._id,
            'deviceInfo.accessToken': token,
        },
        {
            $set: {
                'deviceInfo.$.deviceId': req?.body?.deviceId,
                'deviceInfo.$.deviceType': req?.body?.platform,
                'deviceInfo.$.platform':
                    req?.body?.platform + '/' + req.useragent.source,
            },
        }
    );
    const partner = await Partner.findById(req?.user?._id)
        .populate('zones.zoneId', 'zoneName location')
        .populate(
            'cityId',
            'locationName timezone currencySymbol currencyCode _id orderCalculation'
        )
        .populate('vehicles.vehicleId')
        .lean();
    return res.json(structureUtils.partnerLoginStructure(partner) ?? {});
});

controller.toggleOnlineOffline = handler(async (req, res) => {
    const onlineStatus = req?.user?.onlineStatus;
    if (onlineStatus) {
        await Partner.findByIdAndUpdate(
            req?.user?._id,
            {
                $set: {
                    location: {
                        type: 'Point',
                        coordinates: [req?.body?.lng, req?.body?.lat],
                    },
                    locationUpdatedTime: new Date(),
                    onlineStatus: false,
                    lastOnlineStatusChange: new Date(),
                },
            },
            {
                new: true,
            }
        );
        return res.json({
            onlineStatus: constantUtils.OFFLINE,
        });
    } else {
        if (req?.user?.zones[0]?.zoneId) {
            const ifZone = await Zone.count({
                _id: req?.user?.zones[0]?.zoneId,
                location: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [req?.body?.lng, req?.body?.lat],
                        },
                    },
                },
            });
            if (ifZone === 0)
                throw '400|' + constantUtils.LOCATION_OUTSIDE_ZONE;

            await Partner.findByIdAndUpdate(
                req?.user?._id,
                {
                    $set: {
                        location: {
                            type: 'Point',
                            coordinates: [req?.body?.lng, req?.body?.lat],
                        },
                        locationUpdatedTime: new Date(),
                        onlineStatus: true,
                        lastOnlineStatusChange: new Date(),
                    },
                },
                {
                    new: true,
                }
            );
            return res.json({
                onlineStatus: constantUtils.ONLINE,
            });
        } else throw '400|' + constantUtils.PARTNER_HAS_NO_ZONES;
    }
});

controller.cronPartnerOnlineOfflineStatus = async () => {
    try {
        const config = await Other.config();
        if (config?.partner?.onlineStatusThreshold) {
            const time = subMinutes(
                new Date(),
                config?.partner?.onlineStatusThreshold
            );

            const partners = await Partner.find({
                onlineStatus: true,
                locationUpdatedTime: {
                    $lte: time,
                },
            }).select('firstName lastName email phone');

            if (partners?.length > 0) {
                await Partner.updateMany(
                    {
                        onlineStatus: true,
                        locationUpdatedTime: {
                            $lte: time,
                        },
                    },
                    {
                        $set: {
                            onlineStatus: false,
                            lastOnlineStatusChange: new Date(),
                        },
                    }
                );
                helperUtils.consoleLog(
                    {
                        title: 'No of Partners pushed to OFFLINE',
                        data: partners,
                    },
                    false
                );
            }
        }
    } catch (err) {
        console.log(err);
    }
};

controller.uploadPartnerImage = handler(async (req, res) => {
    let imageUrl = {};
    if (req?.files?.avatar) {
        imageUrl.avatar = await fileUtils.upload(
            req?.files?.avatar[0]?.buffer,
            `PARTNER.avatar.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    if (req?.files?.partnerDocument) {
        imageUrl.partnerDocument = await fileUtils.upload(
            req?.files?.partnerDocument[0]?.buffer,
            `PARTNER.partnerDocument.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    if (req?.files?.frontImage) {
        imageUrl.frontImage = await fileUtils.upload(
            req?.files?.frontImage[0]?.buffer,
            `VEHICLE.frontImage.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    if (req?.files?.backImage) {
        imageUrl.backImage = await fileUtils.upload(
            req?.files?.backImage[0]?.buffer,
            `VEHICLE.backImage.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    if (req?.files?.leftImage) {
        imageUrl.leftImage = await fileUtils.upload(
            req?.files?.leftImage[0]?.buffer,
            `VEHICLE.leftImage.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    if (req?.files?.rightImage) {
        imageUrl.rightImage = await fileUtils.upload(
            req?.files?.rightImage[0]?.buffer,
            `VEHICLE.rightImage.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    if (req?.files?.vehicleDocument) {
        imageUrl.vehicleDocument = await fileUtils.upload(
            req?.files?.vehicleDocument[0]?.buffer,
            `VEHICLE.vehicleDocument.${helperUtils.generateRandom(10, '#')}.jpg`
        );
    }
    return res.json(imageUrl);
});

controller.getEditPartner = handler(async (req, res) => {
    const partner = await Partner.findById(req?.body?.id)
        .populate('vehicles.vehicleId')
        .select('-deviceInfo')
        .lean();
    return res.json(partner);
});

controller.addPartnerAvailableProduct = handler(async (req, res) => {
    const ifProduct = await Product.count({
        _id: req?.body?.productId,
    });
    if (ifProduct === 0) throw '400|' + constantUtils.INVALID_ID;
    const ifPartner = await Partner.count({
        _id: req?.body?.partnerId,
    });
    if (ifPartner === 0) throw '400|' + constantUtils.INVALID_ID;
    else {
        const ifPartnerAlreadyHasProduct = await Partner.count({
            _id: req?.body?.partnerId,
            'availableProducts.productId': req?.body?.productId,
        });
        console.log(ifPartnerAlreadyHasProduct);
        if (ifPartnerAlreadyHasProduct) {
            return res.json({
                message: 'Already In it',
            });
        } else {
            await Partner.findOneAndUpdate(
                {
                    _id: req?.body?.partnerId,
                },
                {
                    $push: {
                        availableProducts: {
                            productId: req?.body?.productId,
                            status: constantUtils.ACTIVE,
                            stockQuantity: req?.body?.stock ?? 0,
                        },
                    },
                }
            );
            return res.json({
                message: 'complete',
            });
        }
    }
});

controller.getPartnerAvailableProducts = handler(async (req, res) => {
    const products = await Partner.findById(req?.body?.id)
        .select('catalogueId availableProducts')
        .populate('availableProducts.productId')
        .lean();
    const catalogueProducts = await Product.find({
        catalogueId: products?.catalogueId,
    }).lean();
    return res.json(products?.availableProducts ?? []);
    // return res.json(
    //     catalogueProducts.map((each) => ({
    //         ...each,
    //         stock:
    //             products?.availableProducts?.find(
    //                 (eachProduct) =>
    //                     eachProduct?.productId?.toString() ===
    //                     each?._id?.toString()
    //             )?.stockQuantity ?? 0,
    //         status:
    //             products?.availableProducts?.find(
    //                 (eachProduct) =>
    //                     eachProduct?.productId?.toString() ===
    //                     each?._id?.toString()
    //             )?.status ?? 'INACTIVE',
    //     }))
    // );
});

controller.partnerProductToggleStatus = handler(async (req, res) => {
    const partner = await Partner.updateOne(
        {
            _id: req?.body?.partnerId,
            'availableProducts.productId': req?.body?.productId,
        },
        {
            $set: {
                'availableProducts.$.status': req?.body?.status,
            },
        }
    );
    // Socket Update Notification
    helperUtils.sendProductUpdatePartner(req?.body?.partnerId, req?.app?.io);

    return res.json({
        message: 'Working !! ',
    });
});

controller.partnerProductAddStock = handler(async (req, res) => {
    const product = await Partner.findOne(
        {
            _id: req?.body?.partnerId,
            'availableProducts.productId': req?.body?.productId,
        },
        {
            'availableProducts.$': 1,
        }
    );

    if (product?.availableProducts?.[0]?.stockQuantity !== undefined) {
        await Partner.updateOne(
            {
                _id: req?.body?.partnerId,
                'availableProducts.productId': req?.body?.productId,
            },
            {
                $set: {
                    'availableProducts.$.stockQuantity': req?.body?.quantity,
                    'availableProducts.$.maxStockQuantity': req?.body?.quantity,
                    'availableProducts.$.lastReFillTime': new Date(),
                },
            }
        );

        // Socket Update Notification
        helperUtils.sendProductUpdatePartner(
            req?.body?.partnerId,
            req?.app?.io
        );

        return res.json({
            message: 'working',
        });
    } else throw '400|' + constantUtils.DATA_NOT_FOUND;
});

controller.updateLocation = handler(async (req, res) => {
    // const ifInsideZone = await Zone.count({
    //     _id: req?.user?.zones[0]?.zoneId,
    //     location: {
    //         $geoIntersects: {
    //             $geometry: {
    //                 type: 'Point',
    //                 coordinates: [req?.body?.lng ?? 0, req?.body?.lat ?? 0],
    //             },
    //         },
    //     },
    // });
    // if (ifInsideZone === 0)
    //     await Partner.findByIdAndUpdate(req?.user?._id, {
    //         $set: {
    //             onlineStatus: false,
    //             lastOnlineStatusChange: new Date(),
    //         },
    //     });

    await Partner.findByIdAndUpdate(req?.user?._id, {
        $set: {
            location: {
                type: 'Point',
                coordinates: [req?.body?.lng, req?.body?.lat],
            },
            locationUpdatedTime: new Date(),
        },
    });

    return res.json({
        message: 'UPDATED',
    });
});

controller.partnerLocationUpdateSocket = async (io, socket, partner, body) => {
    try {
        // const ifInsideZone = await Zone.count({
        //     _id: partner?.zones[0]?.zoneId,
        //     location: {
        //         $geoIntersects: {
        //             $geometry: {
        //                 type: 'Point',
        //                 coordinates: [body?.lng ?? 0, body?.lat ?? 0],
        //             },
        //         },
        //     },
        // });
        // if (ifInsideZone === 0)
        //     await Partner.findByIdAndUpdate(partner?._id, {
        //         $set: {
        //             onlineStatus: false,
        //             lastOnlineStatusChange: new Date(),
        //         },
        //     });
        await Partner.findByIdAndUpdate(partner?._id, {
            $set: {
                location: {
                    type: 'Point',
                    coordinates: [body?.lng, body?.lat],
                },
                locationUpdatedTime: new Date(),
            },
        });
        io?.of(constantUtils.NS.PARTNERS)
            ?.to(socket?.id)
            ?.emit(constantUtils.SOCKET_EVENTS.UPDATE_LOCATION, {
                message: 'Location Updated',
            });
    } catch (err) {
        console.log(err);
    }
};

controller.requestReFill = handler(async (req, res) => {
    if (
        req?.user?.reFillRequest?.status === constantUtils.NEW ||
        req?.user?.reFillRequest?.status === constantUtils.ATTENDED
    )
        throw '400|' + constantUtils.REFILL_REQUEST_ALREADY_PLACED;
    await Partner.updateOne(
        {
            _id: req?.user?._id,
        },
        {
            $set: {
                reFillRequest: {
                    status: constantUtils.NEW,
                    newTime: new Date(),
                },
            },
        }
    );

    // Sockets
    req?.app?.io?.of(constantUtils.NS.ADMINS).emit(req?.user?._id, true);

    Notification.create({
        userType: constantUtils.NOTIFICATIONS.ADMINS,
        notificationType: constantUtils.NOTIFICATIONS.REQUEST_RE_FILL,
        notificationTargetType: constantUtils.ALL,
        notificationContent: {
            partnerId: req?.user?._id,
        },
        completeStatus: false,
    }).then((data) =>
        req?.app?.io
            ?.of(constantUtils.NS.ADMINS)
            .emit(constantUtils.SOCKET_EVENTS.NOTIFICATIONS, data)
    );

    const partner = await Partner.findById(req?.user?._id)
        .populate('zones.zoneId', 'zoneName location')
        .populate(
            'cityId',
            'locationName timezone currencySymbol currencyCode _id orderCalculation'
        )
        .populate('vehicles.vehicleId');

    return res.json(structureUtils.partnerLoginStructure(partner));
});

controller.reFillRequestStatusChange = handler(async (req, res) => {
    if (req?.body?.status === constantUtils.ATTENDED) {
        const ifPartner = await Partner.findOne({
            _id: req?.body?.partnerId,
            'reFillRequest.status': constantUtils.NEW,
        });
        if (!ifPartner) throw '400|' + constantUtils.INVALID_ID;
        await Partner.updateOne(
            {
                _id: req?.body?.partnerId,
                'reFillRequest.status': constantUtils.NEW,
            },
            {
                $set: {
                    'reFillRequest.status': constantUtils.ATTENDED,
                    'reFillRequest.attendedTime': new Date(),
                    'reFillRequest.attendedBy': req?.user?._id,
                },
            }
        );

        await Notification.updateMany(
            {
                notificationType: constantUtils.NOTIFICATIONS.REQUEST_RE_FILL,
                'notificationContent.partnerId': req?.body?.partnerId,
            },
            {
                $set: {
                    completeStatus: true,
                },
            }
        );

        req?.app?.io
            ?.of(constantUtils.NS.ADMINS)
            .emit(req?.body?.partnerId, true);

        return res.json({
            message: 'SUCCESS',
        });
    } else if (req?.body?.status === constantUtils.CLOSED) {
        const partner = await Partner.findOne({
            _id: req?.body?.partnerId,
            'reFillRequest.status': constantUtils.ATTENDED,
        });
        if (!partner) throw '400|' + constantUtils.INVALID_ID;
        await Partner.updateOne(
            {
                _id: req?.body?.partnerId,
                'reFillRequest.status': constantUtils.ATTENDED,
            },
            {
                $set: {
                    'reFillRequest.status': constantUtils.CLOSED,
                    'reFillRequest.closedTime': new Date(),
                },
            }
        );
        req?.app?.io
            ?.of(constantUtils.NS.ADMINS)
            .emit(req?.body?.partnerId, true);

        partner?.deviceInfo?.map((each) => {
            req?.app?.io
                ?.of(constantUtils.NS.PARTNERS)
                ?.to(each?.deliverySocketId)
                .emit(constantUtils.SOCKET_EVENTS?.PROFILE_UPDATE, true);

            if (each?.deviceId)
                fcm.send({
                    userType: constantUtils.PARTNER,
                    platform: 'IOS/undefined',
                    to: each?.deviceId,
                    title: 'Re-Fill Complete',
                    body: 'Admin Closed Re-Fill Request',
                    data: {},
                });
        });

        return res.json({
            message: 'SUCCESS',
        });
    }
    return res.json(req?.body);
});

controller.getPartnerProductsStructure = (partner) => {
    const products = partner?.availableProducts
        ?.filter((each) => each?.productId?.status === constantUtils.ACTIVE)
        ?.map((each, idx) => structureUtils.partnerProductStructure(each));
    return products;
};

controller.getProducts = handler(async (req, res) => {
    const partner = await Partner.findById(req?.user?._id)
        .select('availableProducts')
        .populate('availableProducts.productId');

    return res.json(controller.getPartnerProductsStructure(partner));
});

controller.getDashboard = handler(async (req, res) => {
    let d = new Date();
    d.setHours(0, 0, 0, 0);

    const [totalOrders, deliveredOrders, onGoingOrders, cancelled] =
        await Promise.all([
            Order.count({
                'partner.id': req?.user?._id,
                createdAt: {
                    $gt: d,
                },
            }),
            Order.count({
                'order.status': constantUtils.DELIVERED,
                'partner.id': req?.user?._id,
                createdAt: {
                    $gt: d,
                },
            }),
            Order.count({
                'order.status': {
                    $in: [
                        constantUtils.ACCEPTED,
                        constantUtils.STARTED,
                        constantUtils.DESTINATION_ARRIVED,
                    ],
                },
                'partner.id': req?.user?._id,
                createdAt: {
                    $gt: d,
                },
            }),
            Order.count({
                'order.status': {
                    $in: [
                        constantUtils.PARTNER_CANCELLED,
                        constantUtils.USER_CANCELLED,
                        constantUtils.ADMIN_CANCELLED,
                    ],
                },
                'partner.id': req?.user?._id,
                createdAt: {
                    $gt: d,
                },
            }),
        ]);

    return res.json({
        total: totalOrders,
        pending: onGoingOrders,
        delivered: deliveredOrders,
        cancelled: cancelled,
    });
});

controller.changePassword = handler(async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    const partner = await Partner.findById(req.user._id).lean();
    if (!partner) throw '401|' + constantUtils.INVALID_TOKEN;
    else {
        if (auth.comparePassword(oldPassword, partner.password)) {
            await Partner.findByIdAndUpdate(req.user._id, {
                $set: {
                    password: auth.generatePassword(newPassword),
                },
            });
            // Activity.create({
            //     fromId: user._id,
            //     fromType: constantUtils.ACTIVITIES.USERS,
            //     action: constantUtils.ACTIVITIES.UPDATE_PASSWORD,
            // });
            return res.json({
                code: 200,
                message: constantUtils.SUCCESS,
            });
        } else throw '400|' + constantUtils.OLD_PASSWORD_IS_INVALID;
    }
});

controller.forgotPassword = handler(async (req, res) => {
    const config = await Other.config();
    const baseUrl = config?.adminPanelBaseUrl ?? 'https://www.zervx.com';
    const user = await Partner.findOne({
        email: req.body.email,
    });
    if (user && user.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (!user) throw '404|' + constantUtils.EMAIL_NOT_FOUND;
    const token = uuid();
    await Partner.findByIdAndUpdate(user._id, {
        $set: {
            emailToken: token,
        },
    });

    mail.sendMail(
        user.email,
        'Forgot Password',
        `
        <a href="${baseUrl}/emailVerify/partners/${token}">Click on the Link to Change Password</a>
    `
    );
    // Activity.create({
    //     fromId: user._id,
    //     fromType: constantUtils.ACTIVITIES.USERS,
    //     action: constantUtils.ACTIVITIES.INITIATED_FORGOT_PASSWORD_EMAIL,
    // });
    return res.json({
        message: 'EMAIL_SENT',
    });
});

controller.forgotPasswordEmailGetDetails = handler(async (req, res) => {
    const user = await Partner.findOne({
        emailToken: req?.body?.token,
    });
    if (!user || !req?.body?.token) throw '404|' + constantUtils.INVALID_TOKEN;
    return res.json({
        email: user.email,
    });
});

controller.updateLanguage = handler(async (req, res) => {
    const ifLanguage = await Language.findOne({
        languageCode: req?.body?.languageCode,
    });
    if (!ifLanguage) throw '400|' + constantUtils.LANGUAGE_NOT_FOUND;
    await Partner.findByIdAndUpdate(req?.user?._id, {
        $set: {
            languageCode: req?.body?.languageCode,
        },
    });
    // Activity.create({
    //     fromId: req?.user._id,
    //     fromType: constantUtils.ACTIVITIES.USERS,
    //     action: constantUtils.ACTIVITIES.UPDATE_LANGUAGE,
    // });
    return res.json({
        languageName: ifLanguage?.languageName,
        languageNativeName: ifLanguage?.languageNativeName,
        languageCode: ifLanguage?.languageCode,
        languageKeys: ifLanguage?.languageMobileUserKeys ?? {},
        updateAt: ifLanguage?.updatedAt,
    });
});

controller.availableProductStatusChange = handler(async (req, res) => {
    const condition = {
        _id: req?.user?._id,
        partnerType: constantUtils.STOCK_PARTNER,
        'availableProducts.productId': req?.body?.productId,
    };
    const ifPartner = await Partner.findOne();
    if (!ifPartner) throw '400|' + constantUtils.INVALID_ID;

    await Partner.updateOne(condition, {
        $set: {
            'availableProducts.$.status': req?.body?.status,
        },
    });

    return res.json({
        message: 'SUCCESS',
    });
});

controller.forgotPasswordEmailSubmit = handler(async (req, res) => {
    const ifUser = await Partner.findOne({ emailToken: req.body.token });
    if (!ifUser) throw '400|' + constantUtils.INVALID_TOKEN;

    const password = auth.generatePassword(req.body.password);
    const token = auth.jwtSign({
        _id: ifUser._id,
        type: constantUtils.PARTNER,
        deviceType: req?.headers['device-name'] ?? undefined,
        platform: req?.headers['platform'] ?? undefined,
    });

    const user = await Partner.findByIdAndUpdate(
        ifUser._id,
        {
            $set: {
                emailToken: '',
                password: password,
                emailVerificationTime: new Date(),
                'deviceInfo.accessToken': token,
                // deviceInfo: [
                //     {
                //         accessToken: token,
                //         deviceType: req?.headers['device-name'] ?? undefined,
                //         platform: req?.headers['platform'] ?? undefined,
                //     },
                // ],
            },
        },
        { new: true }
    );
    ifUser?.deviceInfo?.map((each) =>
        req?.app?.io
            ?.of(constantUtils.NS.PARTNERS)
            ?.to(each?.deliverySocketId)
            .emit(constantUtils.SOCKET_EVENTS.PROFILE_UPDATE, true)
    );
    if (user?.deviceInfo?.[0]?.accessToken) {
        // Activity.create({
        //     fromId: user._id,
        //     fromType: constantUtils.ACTIVITIES.USERS,
        //     action: constantUtils.ACTIVITIES.FORGOT_PASSWORD_COMPLETE,
        // });
        return res.json({
            accessToken: user.deviceInfo[0].accessToken,
        });
    } else
        return res.json({
            message: 'Access Token Problem',
        });
});

controller.getPartnersInCity = handler(async (req, res) => {
    const partners = await Partner.find({
        cityId: req?.body?.cityId,
    }).select(
        '-availableProducts -dob -partnerDocuments -gender -vehicles -catalogueId'
    );
    return res.json(partners);
});

controller.orderRequestedPartnerStatusChange = handler(async (req, res) => {
    const order = await Order.count({
        _id: req?.body?.orderId,
        'partner.requestReceived.id': req?.user?._id,
    });
    if (order.length === 0) throw '400|' + constantUtils.INVALID_ID;
    await Order.updateOne(
        {
            _id: req?.body?.orderId,
            'partner.requestReceived.id': req?.user?._id,
        },
        {
            $set: {
                'partner.requestReceived.$.status': req?.body?.status,
            },
        }
    );
    return res.json({
        message: constantUtils.SUCCESS,
    });
});

controller.acceptOrder = handler(async (req, res) => {
    if (!req?.user?.onlineStatus)
        throw '400|' + constantUtils.PARTNER_SHOULD_BE_ONLINE;

    let order = await Order.findOne({
        _id: req?.body?.orderId,
        'order.status': constantUtils.INITIATED,
        'partner.id': { $exists: false },
    }).lean();
    if (!order) throw '400|' + constantUtils.ORDER_ALREADY_ACCEPTED;

    // Checking Maximum Orders
    if (
        (order?.city?.algorithm?.maxOrdersAtTime ?? 1) <=
        (req?.user?.onGoingOrders?.length ?? 0)
    )
        throw '400|' + constantUtils.MAX_ORDERS_REACHED;

    // CHECKING AND UPDATING PARTNER PRODUCTS
    order?.product?.orderedProducts?.forEach((each) => {
        const index = req?.user?.availableProducts?.findIndex(
            (eachProduct) =>
                eachProduct?.productId?.toString() === each?._id?.toString() &&
                eachProduct?.status === constantUtils.ACTIVE
        );
        if (index === -1) throw '400|' + constantUtils.PRODUCT_NOT_AVAILABLE;
        if (
            each?.productType === constantUtils.QUANTITY_STOCK &&
            req?.user?.availableProducts?.[index]?.stockQuantity <
                each?.quantity
        )
            throw '400|' + constantUtils.NOT_ENOUGH_STOCK_QUANTITY;
        else {
            if (each.productType === constantUtils.QUANTITY_STOCK)
                req.user.availableProducts[index].stockQuantity =
                    req?.user?.availableProducts?.[index]?.stockQuantity -
                    each?.quantity;
        }
    });

    // UPDATING ORDER
    const updateData = {};
    updateData['partner.id'] = req?.user?._id;
    updateData['partner.phone.code'] = req?.user?.phone?.code;
    updateData['partner.phone.number'] = req?.user?.phone?.number;
    updateData['activity.accepted.time'] = new Date();
    updateData['activity.accepted.location.lat'] =
        req?.user?.location?.coordinates[1];
    updateData['activity.accepted.location.lng'] =
        req?.user?.location?.coordinates[0];
    if (req?.user?.onGoingOrders?.length) {
        updateData['order.status'] = constantUtils.ACCEPTED;
    } else {
        updateData['order.status'] = constantUtils.STARTED;
        updateData['activity.started.time'] = new Date();
        updateData['activity.started.location.lat'] =
            req?.user?.location?.coordinates[1];
        updateData['activity.started.location.lng'] =
            req?.user?.location?.coordinates[0];
    }
    order = await Order.findByIdAndUpdate(
        {
            _id: req?.body?.orderId,
            'order.status': constantUtils.INITIATED,
            'partner.id': { $exists: false },
        },
        {
            $set: updateData,
        },
        {
            new: true,
        }
    )
        .populate('user.id')
        .populate('partner.id');
    await Partner.findByIdAndUpdate(req?.user?._id, {
        $set: {
            availableProducts: req.user.availableProducts,
        },
        $push: {
            onGoingOrders: {
                orderId: order?._id,
                status: order?.order?.status,
                acceptedTime: new Date(),
            },
        },
    });

    // Notifications
    if (order?.user?.id?._id)
        helperUtils.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            'ACCEPTED ORDER'
        );

    return res.json(structureUtils.orderStructure(order));
});

controller.startOrder = handler(async (req, res) => {
    let order = await Order.findOne({
        _id: req?.body?.orderId,
        'order.status': constantUtils.ACCEPTED,
        'partner.id': req?.user?._id,
    }).populate('user.id');
    if (!order) throw '400|' + constantUtils.INVALID_ID;

    const ifStartedOrder = await Order.count({
        'partner.id': req?.user?._id,
        'order.status': constantUtils.STARTED,
    });

    if (ifStartedOrder) {
        await Order.updateMany(
            {
                'partner.id': req?.user?._id,
                'order.status': constantUtils.STARTED,
            },
            {
                $set: {
                    'order.status': constantUtils.ACCEPTED,
                    'switchedBack.time': new Date(),
                    'switchedBack.location.lat':
                        req?.user?.location?.coordinates[1],
                    'switchedBack.location.lng':
                        req?.user?.location?.coordinates[1],
                },
            }
        );
        await Partner.updateMany(
            {
                _id: req?.user?._id,
                'onGoingOrders.status': constantUtils.STARTED,
            },
            {
                $set: {
                    'onGoingOrders.$.status': constantUtils.ACCEPTED,
                },
            }
        );
    }

    // UPDATING ORDER
    const updateData = {};
    updateData['order.status'] = constantUtils.STARTED;
    updateData['activity.started.time'] = new Date();
    updateData['activity.started.location.lat'] =
        req?.user?.location?.coordinates[1];
    updateData['activity.started.location.lng'] =
        req?.user?.location?.coordinates[1];
    order = await Order.findByIdAndUpdate(
        {
            _id: req?.body?.orderId,
            'order.status': constantUtils.ACCEPTED,
            'partner.id': req?.user?._id,
        },
        {
            $set: updateData,
        },
        {
            new: true,
        }
    )
        .populate('user.id')
        .populate('partner.id');
    await Partner.updateOne(
        {
            _id: req?.user?._id,
            'onGoingOrders.orderId': req?.body?.orderId,
        },
        {
            $set: {
                'onGoingOrders.$.status': constantUtils.STARTED,
            },
        },
        {
            new: true,
        }
    );

    // Notifications
    if (order?.user?.id?._id)
        helperUtils.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            'STARTED ORDER'
        );

    return res.json(structureUtils.orderStructure(order));
});

controller.arriveOrder = handler(async (req, res) => {
    let order = await Order.findOne({
        _id: req?.body?.orderId,
        'order.status': constantUtils.STARTED,
        'partner.id': req?.user?._id,
    }).populate('user.id');
    if (!order) throw '400|' + constantUtils.INVALID_ID;
    // CHECKING IF PARTNER IN ARRIVE RADIUS ZONE
    if (order?.city?.algorithm?.arriveRadius) {
        const destination = turfPoint(
            order?.address?.deliveryLocation?.coordinates
        );
        const partnerLocation = turfPoint(req?.user?.location?.coordinates);
        const distanceBetween =
            turfDistance(destination, partnerLocation) * 1000;
        if (distanceBetween > order?.city?.algorithm?.arriveRadius)
            throw '400|' + constantUtils.NOT_IN_ARRIVE_RADIUS;
    }
    // UPDATING PROFESSIONAL
    await Partner.updateOne(
        {
            _id: req?.user?._id,
            'onGoingOrders.orderId': req?.body?.orderId,
        },
        {
            $set: {
                'onGoingOrders.$.status': constantUtils.DESTINATION_ARRIVED,
            },
        }
    );
    // UPDATING ORDER
    order = await Order.findByIdAndUpdate(
        order?._id,
        {
            $set: {
                'order.status': constantUtils.DESTINATION_ARRIVED,
                'activity.destinationArrived.time': new Date(),
                'activity.destinationArrived.location.lat':
                    req?.user?.location?.coordinates[1],
                'activity.destinationArrived.location.lng':
                    req?.user?.location?.coordinates[0],
            },
        },
        {
            new: true,
        }
    )
        .populate('user.id')
        .populate('partner.id');

    // Notifications
    if (order?.user?.id?._id)
        helperUtils.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            'ARRIVED ORDER'
        );
    return res.json(structureUtils.orderStructure(order));
});

controller.cancelOrder = handler(async (req, res) => {
    let order = await Order.findOne({
        _id: req?.body?.orderId,
        'partner.id': req?.user?._id,
        'order.status': {
            $in: [
                constantUtils.ACCEPTED,
                constantUtils.DESTINATION_ARRIVED,
                constantUtils.STARTED,
            ],
        },
    });
    if (!order) throw '400|' + constantUtils.INVALID_ID;

    // CANCELLATION CHECK
    if (
        order?.city?.orderCalculation?.partner?.cancellation?.status ===
            undefined ||
        order?.city?.orderCalculation?.partner?.cancellation?.status === false
    )
        throw '400|' + constantUtils.CANCELLATION_NOT_AVAILABLE;

    // CANCELLATION THRESHOLD CHECK
    if (
        order?.city?.orderCalculation?.partner?.cancellation?.threshold &&
        -differenceInMinutes(
            new Date(order?.activity?.accepted?.time),
            new Date()
        ) <=
            order?.city?.orderCalculation?.partner?.cancellation?.threshold ===
            false
    )
        throw '400|' + constantUtils.CANCELLATION_TIME_EXCEEDS;

    const updateData = {};

    const updatedProducts = req?.user?.availableProducts?.map((each) => {
        const orderedProduct = order?.product?.orderedProducts?.find(
            (eachProduct) =>
                eachProduct?._id?.toString() === each?.productId?.toString()
        );
        if (orderedProduct && each?.stockQuantity) {
            each['stockQuantity'] =
                each['stockQuantity'] + orderedProduct.quantity;
        }
        return each;
    });

    updateData['order.status'] = constantUtils.PARTNER_CANCELLED;
    updateData['activity.partnerCancelled.time'] = new Date();
    updateData['activity.partnerCancelled.location.lat'] = new Date();
    updateData['activity.partnerCancelled.location.lng'] = new Date();
    updateData['invoice.final.serviceTax'] = 0;
    updateData['invoice.final.deliveryCharge'] = 0;
    updateData['invoice.final.packaging'] = 0;
    updateData['invoice.final.products'] = 0;

    if (
        order?.city?.orderCalculation?.partner?.cancellation?.feeStatus &&
        -differenceInMinutes(
            new Date(order?.activity?.accepted?.time),
            new Date()
        ) >
            order?.city?.orderCalculation?.partner?.cancellation
                ?.feeAfterMinutes
    ) {
        updateData['invoice.final.cancellationCharge'] =
            order?.city?.orderCalculation?.partner?.cancellation?.feeAmount;
        updateData['invoice.final.total'] =
            order?.city?.orderCalculation?.partner?.cancellation?.feeAmount;
    } else {
        updateData['invoice.final.cancellationCharge'] = 0;
        updateData['invoice.final.total'] = 0;
    }

    order = await Order.findByIdAndUpdate(
        req?.body?.orderId,
        {
            $set: updateData,
        },
        {
            new: true,
        }
    )
        .populate('user.id')
        .populate('partner.id');

    await Partner.updateOne(
        {
            _id: req?.user?._id,
        },
        {
            $set: {
                availableProducts: updatedProducts,
            },
            $pull: {
                onGoingOrders: {
                    orderId: order?._id,
                },
            },
        },
        {
            new: true,
        }
    );

    // Updating Partner Products
    helperUtils.sendProductUpdatePartner(req?.user?._id, req?.app?.io);

    // Notifications
    if (order?.user?.id?._id)
        helperUtils.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            'PARTNER CANCELLED ORDER'
        );

    return res.json(structureUtils.orderStructure(order));
});

controller.deliverOrder = handler(async (req, res) => {
    let order = await Order.findOne({
        _id: req?.body?.orderId,
        'partner.id': req?.user?._id,
        'order.status': constantUtils.DESTINATION_ARRIVED,
    })
        .populate('user.id')
        .lean();
    if (!order) throw '400|' + constantUtils.INVALID_ID;

    // UPDATING PARTER
    const partner = await Partner.findByIdAndUpdate(
        req?.user?._id,
        {
            $pull: {
                onGoingOrders: {
                    orderId: order?._id,
                    status: constantUtils.DESTINATION_ARRIVED,
                },
            },
        },
        {
            new: true,
        }
    );

    // UPDATING ORDERS
    order = await Order.findByIdAndUpdate(
        order?._id,
        {
            $set: {
                'order.status': constantUtils.DELIVERED,
                'activity.delivered.time': new Date(),
                'activity.delivered.location.lat':
                    req?.user?.location?.coordinates[1],
                'activity.delivered.location.lng':
                    req?.user?.location?.coordinates[0],
                'invoice.final': order?.invoice?.estimation,
            },
        },
        {
            new: true,
        }
    )
        .populate('user.id')
        .populate('partner.id');

    // Notifications
    if (order?.user?.id?._id)
        helperUtils.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            'DELIVERED ORDER'
        );
    return res.json(structureUtils.orderStructure(order));
});

controller.getOnGoingOrders = handler(async (req, res) => {
    const order = await Order.find({
        'order.status': {
            $in: [constantUtils.ACCEPTED, constantUtils.STARTED],
        },
        'partner.id': req?.user?._id,
    }).populate('user.id');

    return res.json(order?.map((each) => structureUtils.orderStructure(each)));
});

controller.getOrderDetail = handler(async (req, res) => {
    const order = await Order.findById(req?.body?.orderId).populate('user.id');
    if (!order) throw '400|' + constantUtils.INVALID_ID;
    return res.json(structureUtils.orderStructure(order));
});

controller.getOrdersList = handler(async (req, res) => {
    let condition = {
        'partner.id': req?.user?._id,
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.orderType === 'ALL')
        condition['order.status'] = {
            $nin: [constantUtils.INITIATED, constantUtils.EXPIRED],
        };

    if (req?.body?.orderType === 'COMPLETED')
        condition['order.status'] = constantUtils.DELIVERED;

    if (req?.body?.orderType === 'ONGOING')
        condition['order.status'] = {
            $in: [
                constantUtils.ACCEPTED,
                constantUtils.STARTED,
                constantUtils.DESTINATION_ARRIVED,
            ],
        };

    const count = await Order.count(condition);
    const orders = await Order.find(condition)
        .select('-activity')
        .populate('user.id')
        .sort(sort)
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .lean();

    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: orders?.map((each) => structureUtils.orderStructure(each)),
    });
});

module.exports = controller;

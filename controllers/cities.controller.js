const handler = require('express-async-handler');

const City = require('../models/city.model');
const Zone = require('../models/zone.model.js');
const Other = require('../models/other.model');
const Activity = require('../models/activity.model');

const helper = require('../utils/helper.utils');
const constantUtils = require('../utils/constant.utils');

controller = {};

controller.getAllCities = handler(async (req, res) => {
    let condition = {};

    if (req?.body?.search)
        condition['$or'] = helper.generateSearch(
            ['locationName', 'currencySymbol', 'currencyCode', 'timezone'],
            req.body.search
        );
    if (req?.body?.filters?.status !== '')
        condition['status'] = req?.body?.filters?.status;

    const count = await City.count(condition);

    const cities = await City.find(condition)
        .select(
            'locationName currencySymbol currencyCode timezone status createdAt'
        )
        .populate('zones', '_id zoneName')
        .sort('-_id')
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req));

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW_LIST,
        listType: constantUtils.CITIES,
    });

    return res.json({
        totalPages:
            helper.getTotalPages(req, count) === 0
                ? 1
                : helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: cities,
    });
});

controller.getAllCitiesLocations = handler(async (req, res) => {
    const cities = await City.find({}).select('_id locationName location');
    return res.json(cities);
});

controller.addCity = handler(async (req, res) => {
    const condition = {
        $or: [
            {
                locationName: req?.body?.locationName
                    ?.toString()
                    .toLowerCase()
                    .trim(),
            },
            {
                location: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Polygon',
                            coordinates: req?.body?.location ?? [[[0, 0]]],
                        },
                    },
                },
            },
        ],
    };

    if (req?.body?.id) condition['_id'] = { $ne: req?.body?.id };

    const updateData = {
        locationName: req?.body?.locationName,
        currencySymbol: req?.body?.currencySymbol,
        currencyCode: req?.body?.currencyCode,
        timezone: req?.body?.timezone,
        algorithm: req?.body?.algorithm,
        orderCalculation: req?.body?.orderCalculation,
        'location.type': 'Polygon',
        'location.coordinates': req?.body?.location,
        lastEdited: req.user._id,
        lastEditedTime: new Date(),
    };

    const ifCity = await City.findOne(condition);
    if (ifCity) throw '400|' + constantUtils.CITY_ALREADY_EXISTS;

    // Edit
    if (req?.body?.id) {
        const cityBeforeUpdate = await City.findById(req?.body?.id)
            .select('-updatedAt -location -lastEditedTime')
            .lean();
        const cityAfterEdit = await City.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                },
            },
            {
                new: true,
            }
        )
            .select('-updatedAt -location -lastEditedTime')
            .lean();

        // Activities Key Logging
        let editKeys = {};
        editKeys.beforeEditing = helper.differenceInObject(
            JSON.parse(JSON.stringify(cityBeforeUpdate)),
            JSON.parse(JSON.stringify(cityAfterEdit))
        );
        editKeys.afterEditing = helper.differenceInObject(
            JSON.parse(JSON.stringify(cityAfterEdit)),
            JSON.parse(JSON.stringify(cityBeforeUpdate))
        );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
            toId: cityAfterEdit._id,
            toType: constantUtils.ACTIVITIES.CITIES,
            editKeys,
        });
        City.refreshRedis(req?.body?.id);
        // CITY NOTIFICATION UPDATE
        // PARTNER
        req?.app?.io
            ?.of(constantUtils.NS.PARTNERS)
            ?.emit(cityAfterEdit._id, true);
        // USER
        req?.app?.io?.of(constantUtils.NS.USERS)?.emit(cityAfterEdit._id, true);
        return res.json(cityAfterEdit);
    } else {
        const city = await City.create({
            ...updateData,
            addedBy: req.user._id,
            status: constantUtils.ACTIVE,
        });
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.ADD,
            toId: city._id,
            toType: constantUtils.ACTIVITIES.CITIES,
        });
        City.refreshRedis(city?._id);
        return res.json(city);
    }
});

controller.addZone = handler(async (req, res) => {
    const ifCity = await City.count({
        _id: req?.body?.cityId,
    });
    if (ifCity === 0) throw '404|' + constantUtils.CITY_NOT_FOUND;

    let condition = {
        zoneName: req?.body?.zoneName.toString().trim().toLowerCase(),
        cityId: req?.body?.cityId,
    };
    if (req?.body?.id) condition['_id'] = { $ne: req?.body?.id };
    const ifZone = await Zone.count(condition);
    if (ifZone > 0) throw '400|' + constantUtils.NAME_ALREADY_EXISTS;

    const updateData = {
        cityId: req?.body?.cityId,
        zoneName: req?.body?.zoneName,
        'location.type': 'Polygon',
        'location.coordinates': req?.body?.location,
        lastEdited: req?.user?._id,
        lastEditedTime: new Date(),
    };

    if (req?.body?.id) {
        const oldZone = await Zone.findById(req?.body?.id).select('cityId');
        if (!oldZone) throw '404|' + constantUtils.DATA_NOT_FOUND;

        if (oldZone?.cityId !== req?.body?.cityId)
            await City.findByIdAndUpdate(oldZone.cityId, {
                $pull: {
                    zones: oldZone._id,
                },
            });

        const zone = await Zone.findByIdAndUpdate(
            req?.body?.id,
            {
                $set: {
                    ...updateData,
                },
            },
            { new: true }
        );

        await City.findByIdAndUpdate(req?.body?.cityId, {
            $addToSet: {
                zones: zone._id,
            },
        });
        City.refreshRedis(req?.body?.id);
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.UPDATE,
            toId: zone._id,
            toType: constantUtils.ACTIVITIES.ZONES,
        });
        return res.json(zone);
    } else {
        const zone = await Zone.create({
            ...updateData,
            addedBy: req?.user?._id,
            status: constantUtils.ACTIVE,
        });

        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.ADD,
            toId: zone._id,
            toType: constantUtils.ACTIVITIES.ZONES,
        });

        await City.findByIdAndUpdate(req?.body?.cityId, {
            $addToSet: {
                zones: zone._id,
            },
        });

        City.refreshRedis(req?.body?.id);
        return res.json(zone);
    }
});

controller.getZone = handler(async (req, res) => {
    const zone = await Zone.findById(req?.body?.id).populate(
        'cityId',
        '-zones'
    );
    if (!zone) throw '404|' + constantUtils.DATA_NOT_FOUND;
    else return res.json(zone);
});

controller.getCity = handler(async (req, res) => {
    const city = await City.findById(req?.body?.id)
        .populate(
            'lastEdited',
            '_id name data.firstName data.lastName data.avatar data.email data.phone'
        )
        .populate(
            'addedBy',
            '_id name data.firstName data.lastName data.avatar data.email data.phone'
        )
        .lean();
    if (!city) throw '404|' + constantUtils.DATA_NOT_FOUND;

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW,
        toId: city._id,
        toType: constantUtils.ACTIVITIES.CITIES,
    });

    return res.json({
        ...city,
        formattedLocation: city?.location?.coordinates[0]?.map((each) => ({
            lat: each[1],
            lng: each[0],
        })),
    });
});

controller.getAllZones = handler(async (req, res) => {
    let condition = {};
    if (req.body.cityId) condition['cityId'] = req.body.cityId;
    const count = await Zone.count(condition);
    const zones = await Zone.find(condition)
        .select('_id status zoneName createdAt location lastEditedTime')
        .populate(
            'lastEdited',
            '_id name data.firstName data.lastName data.avatar data.email data.phone'
        )
        .populate(
            'addedBy',
            '_id name data.firstName data.lastName data.avatar data.email data.phone'
        )
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .sort('-_id')
        .lean();
    return res.json({
        totalPages:
            helper.getTotalPages(req, count) === 0
                ? 1
                : helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: zones,
    });
});

controller.latInside = handler(async (req, res) => {
    const city = await City.findOne({
        location: {
            $geoIntersects: {
                $geometry: {
                    type: 'Point',
                    coordinates: [req?.body?.lng ?? 0, req?.body?.lat ?? 0],
                },
            },
        },
    }).select('locationName');
    return res.json(city);
});

controller.getAllZonesPolygons = handler(async (req, res) => {
    const zones = await Zone.find({
        cityId: req.body.cityId,
    }).select('zoneName _id location');

    return res.json(zones);
});

module.exports = controller;

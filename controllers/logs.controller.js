const handler = require('express-async-handler');
const { subHours, subDays } = require('date-fns');
const { dot } = require('dot-object');

const Log = require('../models/log.model');
const Other = require('../models/other.model');
const Admin = require('../models/admin.models');
const User = require('../models/user.model');
const Partner = require('../models/partner.model');
const Activity = require('../models/activity.model');

const constantUtils = require('../utils/constant.utils');
const helperUtils = require('../utils/helper.utils');

const controller = {};

controller.getAllLogs = handler(async (req, res) => {
    let condition = {
        $or: helperUtils.generateSearch(
            [
                'data.apiUrl',
                'logType',
                'data.cronType',
                'data.message',
                'data.socketId',
                'data.socketUserData.data.phone.number',
                'data.fcmType',
                'data.apiPlatform',
                'data.errorCode',
            ],
            req?.body?.search || ''
        ),
    };

    let sort = '-_id';

    if (req?.body?.filter === 'API') condition['logType'] = 'API';
    if (req?.body?.filter === 'CRON') condition['logType'] = 'CRON';
    if (req?.body?.filter === 'RESTART_SERVER')
        condition['logType'] = 'RESTART_SERVER';
    if (req?.body?.filter === 'ERROR') condition['logType'] = 'ERROR';
    if (req?.body?.filter === 'SOCKET') condition['logType'] = 'SOCKET';
    if (req?.body?.filter === 'CONSOLE') condition['logType'] = 'CONSOLE';
    if (req?.body?.filter === 'VALIDATION') condition['logType'] = 'VALIDATION';
    if (req?.body?.filter === 'FCM') condition['logType'] = 'FCM';
    if (req?.body?.filter === 'CLIENT_ERROR')
        condition['logType'] = 'CLIENT_ERROR';
    if (req?.body?.sort === 'API_ORDER') sort = '-data.time';

    if (req?.body?.hoursFilter)
        condition['createdAt'] = {
            $gte: subHours(new Date(), req?.body?.hoursFilter),
        };

    if (req?.body?.daysFilter)
        condition['createdAt'] = {
            $gte: subDays(new Date(), req?.body?.daysFilter),
        };

    if (req?.body?.statusCode)
        condition['data.apiStatusCode'] = req?.body?.statusCode;

    if (req?.body?.socketConnectionType)
        condition['data.socketConnectionType'] =
            req?.body?.socketConnectionType;

    const [statusCodes, count, allLogs] = await Promise.all([
        Log.collection.distinct('data.apiStatusCode'),
        Log.count(condition),
        Log.find(condition)
            .sort(sort)
            .skip(helperUtils.getSkip(req))
            .limit(helperUtils.getLimit(req))
            .lean(),
    ]);

    if (req?.body?.filter === '')
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            action: constantUtils.ACTIVITIES.VIEW_LIST,
            listType: constantUtils.LOGS,
        });

    return res.json({
        statusCodes,
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: allLogs,
    });
});

controller.getApiLogCount = handler(async (req, res) => {
    let condition = [];
    condition.push({ $match: { logType: 'API' } });
    condition.push({
        $group: {
            _id: '$data.apiUrl',
            count: { $sum: 1 },
            statusCode: { $addToSet: '$data.apiStatusCode' },
            date: { $last: '$createdAt' },
        },
    });
    condition.push({
        $sort: {
            count: -1,
        },
    });
    const count = await Log.aggregate([
        ...condition,
        {
            $count: 'count',
        },
    ]);
    const allLogs = await Log.aggregate([
        ...condition,
        {
            $skip: helperUtils.getSkip(req),
        },
        {
            $limit: helperUtils.getLimit(req),
        },
    ]);
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count[0].count),
        totalDocs: count[0].count,
        currentPage: helperUtils.getCurrentPage(req),
        data: allLogs,
    });
});

controller.maintainLogsLimit = async () => {
    const config = await Other.config();
    const limit = config?.maintainDocs?.logsDocsLimit ?? 1000;
    const count = await Log.count();
    if (count - limit > 0) {
        const idToDelete = await Log.find()
            .select('_id')
            .limit(count - limit);
        await Log.deleteMany({
            _id: { $in: idToDelete },
        });
    }
};

controller.socketLog = async (socketId, userType, connectType) => {
    if (userType === 'ADMIN') {
        const admin = await Admin.findOne({
            'data.socketId': socketId,
        }).select('_id data.phone data.firstName data.lastName data.email');
        Log.create({
            logType: 'SOCKET',
            data: {
                socketUserType: 'ADMIN',
                socketConnectionType: connectType,
                socketId: socketId,
                socketUserData: admin,
            },
        });
    }
    if (userType === 'USER') {
        const user = await User.findOne({
            'deviceInfo.deliverySocketId': socketId,
        }).select('_id phone firstName lastName email avatar');
        Log.create({
            logType: 'SOCKET',
            data: {
                socketUserType: 'USER',
                socketConnectionType: connectType,
                socketId: socketId,
                socketUserData: user,
            },
        });
    }
    if (userType === 'PARTNER') {
        const partner = await Partner.findOne({
            'deviceInfo.deliverySocketId': socketId,
        }).select('_id phone firstName lastName email avatar');
        Log.create({
            logType: 'SOCKET',
            data: {
                socketUserType: 'PARTNER',
                socketConnectionType: connectType,
                socketId: socketId,
                socketUserData: partner,
            },
        });
    }
};

module.exports = controller;

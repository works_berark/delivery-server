const handler = require('express-async-handler');
const { v4: uuid } = require('uuid');
const mongoose = require('mongoose');
const differenceInMinutes = require('date-fns/differenceInMinutes');

const User = require('../models/user.model');
const Other = require('../models/other.model');
const Activity = require('../models/activity.model');
const Language = require('../models/language.model.js');
const Product = require('../models/product.model');
const Partner = require('../models/partner.model');
const City = require('../models/city.model');
const Shop = require('../models/shop.model');
const Rating = require('../models/rating.model');
const Zone = require('../models/zone.model');
const Order = require('../models/order.model');

const turf = require('@turf/turf');

const auth = require('../utils/auth.utils');

const structureUtils = require('../utils/structure.utils');
const smsUtils = require('../utils/sms.utils');
const helper = require('../utils/helper.utils');
const mail = require('../utils/mail.utils');
const fileUtils = require('../utils/upload.utils');
const calculationUtils = require('../utils/calculation.utils');

const constantUtils = require('../utils/constant.utils');

const controller = {};

controller.getAllUsers = handler(async (req, res) => {
    let condition = {
        $or: helper.generateSearch(
            ['firstName', 'lastName', 'email', 'phone.number', 'phone.code'],
            req.body.search
        ),
    };

    // Sort
    let sort = '-createdAt';

    // Filters
    if (req?.body?.filters?.status)
        condition['status'] = req.body.filters.status;
    if (req?.body?.filters?.gender)
        condition['gender'] = req.body.filters.gender;
    if (req?.body?.filters?.startDate)
        condition['createdAt'] = {
            $gte: req?.body.filters?.startDate,
        };
    if (req?.body?.filters?.endDate)
        condition['createdAt'] = {
            $lte: req?.body?.filters?.endDate,
        };
    if (req?.body?.filters?.phoneNumberVerifiedUsers === 'VERIFIED')
        condition['phoneNumberVerifiedTime'] = {
            $exists: true,
        };
    if (req?.body?.filters?.phoneNumberVerifiedUsers === 'UNVERIFIED')
        condition['phoneNumberVerifiedTime'] = {
            $exists: false,
        };

    const count = await User.count(condition);
    const operators = await User.find(condition)
        .select(
            'firstName lastName rating email phone avatar phoneNumberVerifiedTime createdAt status'
        )
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW_LIST,
        listType: constantUtils.USERS,
    });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: operators.map((each) => ({
            id: each._id,
            name: each?.firstName + ' ' + each?.lastName,
            email: each?.email,
            phoneCode: each?.phone.code,
            phoneNumber: each?.phone.number,
            avatar: each?.avatar,
            numberVerified: each?.phoneNumberVerifiedTime ? true : false,
            createdAt: each?.createdAt,
            rating: each?.rating,
            status: each?.status,
        })),
    });
});

controller.getViewUser = handler(async (req, res) => {
    const [user, recentRatings] = await Promise.all([
        User.findById(req.body.id)
            .select('-password')
            .populate(
                'statusLastEdited',
                'data.firstName _id data.lastName data.avatar'
            )
            .lean(),
        Rating.find({
            userId: req?.body?.id,
            ratingFor: constantUtils.USER,
        })
            .populate('partnerId', 'firstName lastName phone email')
            .sort('-_id')
            .limit(5)
            .lean(),
    ]);
    user.recentRatings = recentRatings;
    if (!user) throw '400|' + constantUtils.INVALID_ID;
    return res.json(user);
});

controller.getEditUser = handler(async (req, res) => {
    const user = await User.findById(req.body.id)
        .select('-password')
        .populate(
            'statusLastEdited',
            'data.firstName _id data.lastName data.avatar'
        );
    if (user) {
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: req.body.id,
            toType: constantUtils.ACTIVITIES.USERS,
            action: constantUtils.ACTIVITIES.VIEW,
        });
        return res.json(user);
    } else throw '404|' + constantUtils.INVALID_ID;
});

controller.changeUserStatus = handler(async (req, res) => {
    await User.updateMany(
        {
            _id: { $in: req.body.id },
        },
        {
            $set: {
                status: req.body.status,
                statusLastEdited: req.user._id,
            },
        }
    );
    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        toId: req.body.id,
        toType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.STATUS_CHANGE,
    });
    req?.body?.id?.map(async (each) => {
        const user = await User.findById(each).select('deviceInfo');
        user?.deviceInfo?.map((eachInfo) =>
            req?.app?.io
                ?.of(constantUtils.NS.USERS)
                ?.to(eachInfo?.deliverySocketId)
                .emit(constantUtils.SOCKET_EVENTS.PROFILE_UPDATE, true)
        );
    });
    return res.json({
        message: 'Success',
    });
});

// Mobile Registeration

controller.registerCheckEmail = handler(async (req, res) => {
    const ifUser = await User.findOne({
        email: req.body.email,
    });

    if (ifUser && ifUser.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (ifUser) {
        Activity.create({
            fromId: ifUser._id,
            fromType: constantUtils.ACTIVITIES.USERS,
            action: constantUtils.ACTIVITIES.REGISTER_EMAIL_CHECK,
        });

        return res.json({
            emailValid: true,
        });
    } else
        return res.json({
            emailValid: false,
        });
});

controller.registerPhoneNumberSendOTP = handler(async (req, res) => {
    let ifUser = await User.findOne({
        'phone.number': req.body.phoneNumber,
    });

    if (ifUser && ifUser.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (ifUser && ifUser.phoneNumberVerifiedTime)
        throw '400|' + constantUtils.PHONE_ALREADY_VERIFIED;
    else if (ifUser && ifUser?.phone?.code !== req.body.phoneCode)
        throw (
            '400|' +
            constantUtils.PHONE_NUMBER_ALREADY_REGISTERED_WITH_ANOTHER_PHONE_CODE
        );

    const config = await Other.config();
    const otpLength = config?.otpLength ?? 5;
    let otp = helper.generateRandom(otpLength, '#');

    if (ifUser && ifUser.phoneNumberOtp) otp = ifUser.phoneNumberOtp;
    const updateData = {
        'phone.code': req.body.phoneCode,
        'phone.number': req.body.phoneNumber,
        phoneNumberOtp: otp,
    };
    if (ifUser)
        await User.findByIdAndUpdate(ifUser._id, {
            $set: updateData,
        });
    else {
        ifUser = await User.create(updateData);
    }
    smsUtils.send({
        to: req.body.phoneCode + req.body.phoneNumber,
        body: otp,
    });
    Activity.create({
        fromId: ifUser._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.SEND_OTP,
    });
    return res.json({
        message: 'SMS Sent',
        otp: config?.otpMode === 'DEVELOPMENT' ? otp : undefined,
    });
});

controller.registerVerifyOTP = handler(async (req, res) => {
    const ifUser = await User.findOne({
        'phone.code': req.body.phoneCode,
        'phone.number': req.body.phoneNumber,
        phoneNumberOtp: req.body.otp,
    });
    if (ifUser && ifUser.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (ifUser)
        return res.json({
            otpValid: true,
        });
    else
        return res.json({
            otpValid: false,
        });
});

controller.registerNewUser = handler(async (req, res) => {
    const ifEmail = await User.findOne({
        email: req.body.email,
    });
    if (ifEmail) throw '400|' + constantUtils.EMAIL_ALREADY_EXISTS;
    const ifValidOTP = await User.findOne({
        'phone.code': req.body.phoneCode,
        'phone.number': req.body.phoneNumber,
        phoneNumberOtp: req.body.otp,
    });
    if (!ifValidOTP) throw '400|' + constantUtils.INVALID_OTP;
    if (ifValidOTP.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    const token = auth.jwtSign({
        _id: ifValidOTP._id,
        type: constantUtils.USER,
        deviceType: req?.body?.platform,
        platform: req?.body?.platform + '/' + req?.useragent?.source,
    });
    const password = auth.generatePassword(req.body.password);
    const user = await User.findByIdAndUpdate(
        ifValidOTP._id,
        {
            $set: {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: password,
                phoneNumberOtp: '',
                phoneNumberVerifiedTime: new Date(),
                deviceInfo: [
                    {
                        accessToken: token,
                        deviceId: req?.body?.deviceId,
                        deviceType: req?.body?.platform,
                        platform:
                            req?.body?.platform + '/' + req?.useragent?.source,
                    },
                ],
            },
        },
        { new: true }
    );
    Activity.create({
        fromId: user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.REGISTRATION_COMPLETE,
    });
    Activity.create({
        fromId: user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.OTP_VERIFIED,
    });
    return res.json(structureUtils.userLoginStructure(user));
});

controller.emailPasswordLogin = handler(async (req, res) => {
    const ifUser = await User.findOne({
        email: req.body.email,
    }).select('password status deviceInfo');
    if (ifUser && ifUser.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (!ifUser || !ifUser.password)
        throw '404|' + constantUtils.INVALID_CREDENTIALS;
    else {
        if (auth.comparePassword(req.body.password, ifUser.password)) {
            const oldDeviceInfo = ifUser?.deviceInfo;
            const token = auth.jwtSign({
                _id: ifUser._id,
                type: constantUtils.USER,
                deviceType: req?.body?.platform,
                platform: req?.body?.platform + '/' + req?.useragent?.source,
            });
            const [logedInUser, onGoingOrders] = await Promise.all([
                User.findByIdAndUpdate(
                    ifUser._id,
                    {
                        $set: {
                            deviceInfo: [
                                {
                                    accessToken: token,
                                    deviceId: req?.body?.deviceId,
                                    deviceType: req?.body?.platform,
                                    platform:
                                        req?.body?.platform +
                                        '/' +
                                        req?.useragent?.source,
                                },
                            ],
                        },
                    },
                    { new: true }
                ).lean(),
                Order.find({
                    'user.id': ifUser._id,
                    'order.status': {
                        $in: [
                            constantUtils.ACCEPTED,
                            constantUtils.STARTED,
                            constantUtils.DESTINATION_ARRIVED,
                        ],
                    },
                })
                    .select('order')
                    .lean(),
            ]);
            logedInUser.onGoingOrders = onGoingOrders;
            Activity.create({
                fromId: logedInUser._id,
                fromType: constantUtils.ACTIVITIES.USERS,
                action: constantUtils.ACTIVITIES.LOG_IN,
            });
            oldDeviceInfo?.map((each) =>
                req?.app?.io
                    ?.of(constantUtils.NS.USERS)
                    ?.to(each?.deviceId)
                    .emit(constantUtils.SOCKET_EVENTS.PROFILE_UPDATE, true)
            );
            return res.json(structureUtils.userLoginStructure(logedInUser));
        } else throw '404|' + constantUtils.INVALID_CREDENTIALS;
    }
});

controller.forgotPasswordEmail = handler(async (req, res) => {
    const config = await Other.config();
    const baseUrl = config?.adminPanelBaseUrl ?? 'https://www.zervx.com';
    const user = await User.findOne({
        email: req.body.email,
    });
    if (user && user.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (!user) throw '404|' + constantUtils.EMAIL_NOT_FOUND;
    const token = uuid();
    await User.findByIdAndUpdate(user._id, {
        $set: {
            emailToken: token,
        },
    });

    mail.sendMail(
        user.email,
        'Forgot Password',
        `
        <a href="${baseUrl}/emailVerify/users/${token}">Click on the Link to Change Password</a>
    `
    );
    Activity.create({
        fromId: user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.INITIATED_FORGOT_PASSWORD_EMAIL,
    });
    return res.json({
        message: 'EMAIL_SENT',
    });
});

controller.forgotPasswordEmailGetDetails = handler(async (req, res) => {
    const user = await User.findOne({
        emailToken: req.body.token,
    });
    if (!user) throw '404|' + constantUtils.INVALID_TOKEN;
    return res.json({
        email: user.email,
    });
});

controller.forgotPasswordEmailSubmit = handler(async (req, res) => {
    const ifUser = await User.findOne({ emailToken: req.body.token });
    if (!ifUser) throw '400|' + constantUtils.INVALID_TOKEN;

    const password = auth.generatePassword(req.body.password);
    const token = auth.jwtSign({
        _id: ifUser._id,
        type: constantUtils.USER,
        deviceType: req?.headers['device-name'] ?? req.useragent.platform,
        platform: req?.headers['platform'] ?? req.useragent.platform,
    });

    const user = await User.findByIdAndUpdate(
        ifUser._id,
        {
            $set: {
                emailToken: '',
                password: password,
                emailVerificationTime: new Date(),
                'deviceInfo.0.accessToken': token,
                // deviceInfo: [
                //     {
                //         accessToken: token,
                //         deviceType:
                //             req?.headers['device-name'] ??
                //             req.useragent.platform,
                //         platform:
                //             req?.headers['platform'] ?? req.useragent.platform,
                //     },
                // ],
            },
        },
        { new: true }
    );
    ifUser?.deviceInfo?.map((each) =>
        req?.app?.io
            ?.of(constantUtils.NS.USERS)
            ?.to(each?.deliverySocketId)
            .emit(constantUtils.SOCKET_EVENTS.PROFILE_UPDATE, true)
    );
    if (user?.deviceInfo?.[0]?.accessToken) {
        Activity.create({
            fromId: user._id,
            fromType: constantUtils.ACTIVITIES.USERS,
            action: constantUtils.ACTIVITIES.FORGOT_PASSWORD_COMPLETE,
        });
        return res.json({
            accessToken: user.deviceInfo[0].accessToken,
        });
    } else
        return res.json({
            message: 'Access Token Problem',
        });
});

controller.getDetailsFromToken = handler(async (req, res) => {
    const user = await User.findOne({
        'deviceInfo.accessToken': req.body.accessToken,
    });
    if (user && user.status !== constantUtils.ACTIVE)
        throw '400|' + constantUtils.RESTRICTED_ACCOUNT_CONTACT_SUPPORT;
    if (!user) throw '401|' + constantUtils.INVALID_TOKEN;
    else return res.json(structureUtils.userLoginStructure(user));
});

controller.checkUserLogin = handler(async (req, res) => {
    return res.json(req?.user);
});

controller.updateProfile = handler(async (req, res) => {
    const updateData = {
        firstName: req?.body?.firstName,
        lastName: req?.body?.lastName,
        email: req?.body?.email,
    };

    if (req.file && req?.file?.buffer) {
        if (!req?.file?.mimetype.includes('image'))
            throw '400|' + constantUtils.INVALID_IMAGE;
        if (req?.user?.avatar) await fileUtils.delete(req?.user?.avatar);
        updateData['avatar'] = await fileUtils.upload(
            req?.file?.buffer,
            `${constantUtils.USERS}.${
                req?.user?.phone?.number
            }.${helper.generateRandom(5, '#')}.jpg`
        );
    }

    const ifUserExists = await User.findOne({
        _id: {
            $ne: req?.user?._id,
        },
        email: req.body.email,
    });

    if (ifUserExists) throw '400|' + constantUtils.EMAIL_ALREADY_EXISTS;

    let user;

    user = await User.findByIdAndUpdate(
        req?.user?._id,
        {
            $set: {
                ...updateData,
            },
        },
        {
            new: true,
        }
    );

    if (req?.user?.email !== req?.body?.email) {
        const config = await Other.config();
        const baseUrl = config?.apiBaseUrl ?? 'https://www.zervx.com';

        const token = uuid();
        user = await User.findByIdAndUpdate(
            req?.user._id,
            {
                $set: {
                    emailToken: token,
                },
                $unset: {
                    emailVerificationTime: 1,
                },
            },
            {
                new: true,
            }
        ).lean();

        Activity.create({
            fromId: user._id,
            fromType: constantUtils.ACTIVITIES.USERS,
            action: constantUtils.ACTIVITIES.INITIATED_VERIFY_EMAIL,
        });

        mail.sendMail(
            req?.body?.email,
            'Verify Email',
            `
        <a href="${baseUrl}/api/delivery/users/verifyToken/${token}">Click on the Link to Verify Email</a>
    `
        );
    }

    const onGoingOrders = await Order.find({
        'user.id': user._id,
        'order.status': {
            $in: [
                constantUtils.ACCEPTED,
                constantUtils.STARTED,
                constantUtils.DESTINATION_ARRIVED,
            ],
        },
    })
        .select('order')
        .lean();
    console.log(onGoingOrders);
    user.onGoingOrders = onGoingOrders;

    Activity.create({
        fromId: user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.UPDATE_PROFILE,
    });

    return res.json(structureUtils.userLoginStructure(user));
});

controller.getProfile = handler(async (req, res) => {
    const token = req?.headers?.authorization?.split(' ')?.[1];
    await User.updateOne(
        {
            _id: req?.user?._id,
            'deviceInfo.accessToken': token,
        },
        {
            $set: {
                'deviceInfo.$.deviceId': req?.body?.deviceId,
                'deviceInfo.$.deviceType': req?.body?.platform,
                'deviceInfo.$.platform':
                    req?.body?.platform + '/' + req.useragent.source,
            },
        }
    );
    const onGoingOrders = await Order.find({
        'user.id': req?.user?._id,
        'order.status': {
            $in: [
                constantUtils.ACCEPTED,
                constantUtils.STARTED,
                constantUtils.DESTINATION_ARRIVED,
            ],
        },
    })
        .select('order')
        .lean();
    req.user.onGoingOrders = onGoingOrders;

    return res.json(structureUtils.userLoginStructure(req?.user));
});

controller.emailVerify = handler(async (req, res) => {
    if (req?.user?.emailVerificationTime)
        throw '400|' + constantUtils.EMAIL_ALREADY_VERIFIED;

    const config = await Other.config();
    const baseUrl = config?.apiBaseUrl ?? 'https://www.zervx.com';

    const token = uuid();
    await User.findByIdAndUpdate(req?.user._id, {
        $set: {
            emailToken: token,
        },
    });

    mail.sendMail(
        req?.user.email,
        'Verify Email',
        `
        <a href="${baseUrl}/api/delivery/users/verifyToken/${token}">Click on the Link to Verify Email</a>
    `
    );
    Activity.create({
        fromId: req?.user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.INITIATED_VERIFY_EMAIL,
    });
    return res.json({
        message: 'EMAIL_SENT',
    });
});

controller.verifyToken = handler(async (req, res) => {
    const user = await User.findOne({
        emailToken: req?.params?.token,
    }).select('_id email deviceInfo');
    if (!user) return res.send(`<h1>Invalid Token</h1>`);
    await User.findByIdAndUpdate(user._id, {
        $set: {
            emailVerificationTime: new Date(),
            emailToken: '',
        },
    });
    Activity.create({
        fromId: user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.EMAIL_VERIFIED,
    });
    user?.deviceInfo?.map((each) =>
        req?.app?.io
            ?.of(constantUtils.NS?.USERS)
            ?.to(each?.deliverySocketId)
            ?.emit(constantUtils.SOCKET_EVENTS?.PROFILE_UPDATE, true)
    );
    return res.send(`<h1>Email : ${user?.email} Verified </h1>`);
});

controller.updateLanguage = handler(async (req, res) => {
    const ifLanguage = await Language.findOne({
        languageCode: req?.body?.languageCode,
    });
    if (!ifLanguage) throw '400|' + constantUtils.LANGUAGE_NOT_FOUND;
    await User.findByIdAndUpdate(req?.user?._id, {
        $set: {
            languageCode: req?.body?.languageCode,
        },
    });
    Activity.create({
        fromId: req?.user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.UPDATE_LANGUAGE,
    });
    return res.json({
        languageName: ifLanguage?.languageName,
        languageNativeName: ifLanguage?.languageNativeName,
        languageCode: ifLanguage?.languageCode,
        languageKeys: ifLanguage?.languageMobileUserKeys ?? {},
        updateAt: ifLanguage?.updatedAt,
    });
});

controller.changePassword = handler(async (req, res) => {
    const { oldPassword, newPassword } = req.body;
    const user = await User.findById(req.user._id).lean();
    if (!user) throw '401|' + constantUtils.INVALID_TOKEN;
    else {
        if (auth.comparePassword(oldPassword, user.password)) {
            await User.findByIdAndUpdate(req.user._id, {
                $set: {
                    password: auth.generatePassword(newPassword),
                },
            });
            Activity.create({
                fromId: user._id,
                fromType: constantUtils.ACTIVITIES.USERS,
                action: constantUtils.ACTIVITIES.UPDATE_PASSWORD,
            });
            return res.json({
                code: 200,
                message: constantUtils.SUCCESS,
            });
        } else throw '400|' + constantUtils.OLD_PASSWORD_IS_INVALID;
    }
});

controller.toggleFavorites = handler(async (req, res) => {
    const ifProduct = await Product.count({
        _id: req?.body?.id,
    });
    if (ifProduct === 0) throw '400|' + constantUtils.INVALID_ID;

    const ifInsideFavorite = await User.count({
        _id: req?.user._id,
        'favoriteProducts.productId': req?.body?.id,
    });

    if (ifInsideFavorite > 0) {
        await User.findByIdAndUpdate(req?.user?._id, {
            $pull: {
                favoriteProducts: { productId: req?.body?.id },
            },
        });
        Activity.create({
            fromId: req?.user._id,
            fromType: constantUtils.ACTIVITIES.USERS,
            action: constantUtils.ACTIVITIES.UPDATE_FAVORITES,
        });
        return res.json({
            message: 'PULLED',
        });
    } else {
        await User.findByIdAndUpdate(req?.user?._id, {
            $push: {
                favoriteProducts: {
                    productId: req?.body?.id,
                    favoredTime: new Date(),
                },
            },
        });
        Activity.create({
            fromId: req?.user._id,
            fromType: constantUtils.ACTIVITIES.USERS,
            action: constantUtils.ACTIVITIES.UPDATE_FAVORITES,
        });
        return res.json({
            message: 'PUSHED',
        });
    }
});

controller.getFavorites = handler(async (req, res) => {
    const favorites = await User.findById(req?.user?._id).populate(
        'favoriteProducts.productId'
    );
    Activity.create({
        fromId: req?.user._id,
        fromType: constantUtils.ACTIVITIES.USERS,
        action: constantUtils.ACTIVITIES.VIEW_FAVORITES,
    });
    return res.json(
        favorites?.favoriteProducts?.map((eachProduct) => ({
            id: eachProduct?.productId?._id,
            name: eachProduct?.productId?.name,
            contentType: 'PRODUCT',
            image: eachProduct?.productId?.image,
            price: eachProduct?.productId?.price,
            description: eachProduct?.productId?.description,
            vegNonVeg: eachProduct?.productId?.vegNonVeg,
        })) ?? []
    );
});

controller.getNearByPartners = handler(async (req, res) => {
    const from = {
        type: 'Point',
        coordinates: [req?.body?.lng, req?.body?.lat],
    };
    const partners = await Partner.find({
        status: constantUtils.ACTIVE,
        location: {
            $near: {
                $geometry: from,
                $maxDistance: req?.body.distance,
            },
        },
    }).select('firstName lastName email location avatar onlineStatus');
    return res.json(
        partners.map((each) => ({
            id: each?._id,
            firstName: each?.firstName,
            lastName: each?.lastName,
            email: each?.email,
            location: {
                lat: each?.location?.coordinates?.[1],
                lng: each?.location?.coordinates?.[0],
            },
            onlineStatus: each?.onlineStatus,
            avatar: each?.avatar,
            distance:
                parseFloat(turf.distance(from, each?.location))?.toFixed(2) +
                ' KM',
        }))
    );
});

controller.getCheckOutAmount = handler(async (req, res) => {
    const city = await City.getCity(req?.body?.cityId);
    if (!city) throw '400|' + constantUtils.DELIVERY_UNAVAILABLE;

    if (city?.algorithm?.partnerType === constantUtils.STOCK_PARTNER) {
        const shop = await Shop.getShop(req?.body?.shopId);
        if (!shop) throw '400|' + constantUtils.INVALID_SHOP_ID;
        const { products, totalProductAmount, packaging, totalQuantity } =
            await calculationUtils.calculateProducts(req.body.products, city);
        const tax = await calculationUtils.calculateTax(
            totalProductAmount,
            city
        );
        const { deliveryCharge, nightFare } =
            await calculationUtils.calculateDeliveryCharge(city, new Date());
        return res.json({
            acceptPaymentModes: city?.algorithm?.acceptPaymentModes,
            total:
                deliveryCharge +
                tax +
                nightFare +
                packaging +
                totalProductAmount,
            cost: [
                {
                    label: await helper.getLanguageKey(
                        constantUtils.DELIVERY_CHARGE,
                        req?.headers['language-code'] || 'en'
                    ),
                    bold: false,
                    value: deliveryCharge,
                },
                {
                    label: await helper.getLanguageKey(
                        constantUtils.NIGHT_FARE,
                        req?.headers['language-code'] || 'en'
                    ),
                    bold: false,
                    value: nightFare,
                },
                {
                    label:
                        (await helper.getLanguageKey(
                            constantUtils.TAX,
                            req?.headers['language-code'] || 'en'
                        )) +
                        ` ${city.orderCalculation.serviceTaxPercentage ?? 0}%`,
                    bold: true,
                    value: tax,
                },
                {
                    label:
                        (await helper.getLanguageKey(
                            constantUtils.PACKAGING,
                            req?.headers['language-code'] || 'en'
                        )) +
                        ` (${
                            city.orderCalculation.shop.packagePerProduct ?? 0
                        } X ${totalQuantity})`,
                    bold: false,
                    value: packaging,
                },
                {
                    label: await helper.getLanguageKey(
                        constantUtils.TOTAL_PRODUCT_AMOUNT,
                        req?.headers['language-code'] || 'en'
                    ),
                    bold: false,
                    value: totalProductAmount,
                },
            ],
            products: products.map((each) =>
                structureUtils.productStructure(each)
            ),
        });
    } else throw '400|' + constantUtils.CITY_NOT_YET_READY;
});

controller.placeOrder = handler(async (req, res) => {
    const config = await Other.config();
    // Getting City
    const city = await City.getCityFromLatLng(
        req?.body?.deliveryAddress?.lat,
        req?.body?.deliveryAddress?.lng
    );
    if (!city) throw '400|' + constantUtils.DELIVERY_UNAVAILABLE;
    // Checking Shop
    const shop = await Shop.getShop(req?.body?.shopId);
    if (!shop) throw '400|' + constantUtils.INVALID_SHOP_ID;
    // Getting Zone
    const zone = await Zone.getZoneFromLatLng(
        req?.body?.deliveryAddress?.lat,
        req?.body?.deliveryAddress?.lng
    );
    const { products, packaging, totalProductAmount, totalQuantity } =
        await calculationUtils.calculateProducts(req?.body?.products, city);
    // Getting Partner Info
    let partners = [];
    let partnerCondition = {};
    partnerCondition['$and'] = [];
    const singleCondition = {
        onlineStatus: true,
        partnerType: constantUtils.STOCK_PARTNER,
        status: constantUtils.ACTIVE,
    };
    if (city?.algorithm?.maxOrdersAtTime) {
        singleCondition[
            `onGoingOrders.${city?.algorithm?.maxOrdersAtTime - 1}`
        ] = {
            $exists: false,
        };
    }
    partnerCondition['$and'].push(singleCondition);
    products?.forEach((each) => {
        partnerCondition['$and'].push({
            availableProducts: {
                $elemMatch: {
                    productId: each?._id,
                    status: 'ACTIVE',
                    stockQuantity:
                        each?.productType === constantUtils.QUANTITY_STOCK
                            ? {
                                  $gte: each.quantity,
                              }
                            : undefined,
                },
            },
        });
    });

    // If Delivery Location in placed inside Zone
    if (city?.algorithm?.region?.zoneOrNearBy === constantUtils.ZONES && zone) {
        partnerCondition['$and'].push({
            'zones.zoneId': zone?._id,
            location: {
                $geoWithin: {
                    $geometry: zone?.location,
                },
            },
        });
        partners = await Partner.find(partnerCondition);
        if (partners.length === 0)
            throw '400|' + constantUtils.NO_PARTNERS_AVAILABLE;
    } else {
        partners = await Partner.aggregate([
            {
                $geoNear: {
                    near: {
                        type: 'Point',
                        coordinates: [
                            req?.body?.deliveryAddress?.lng,
                            req?.body?.deliveryAddress?.lat,
                        ],
                    },
                    distanceField: 'dist.calculated',
                    maxDistance: city?.algorithm?.region?.nearByRadius ?? 0,
                    spherical: true,
                },
            },
            { $match: partnerCondition },
        ]);
        if (partners.length === 0)
            throw '400|' + constantUtils.NO_PARTNERS_AVAILABLE;
    }

    const tax = await calculationUtils.calculateTax(totalProductAmount, city);
    const { deliveryCharge, nightFare } =
        await calculationUtils.calculateDeliveryCharge(city, new Date());

    let order = await Order.create({
        'order.uniqueId':
            (config?.order?.prefix ?? '') + '-' + helper.generateRandom(9, '#'),
        'order.orderType': constantUtils.INSTANT,
        'order.otp': helper.generateRandom(config?.otpLength ?? 5, '#'),
        'order.status': constantUtils.INITIATED,
        'order.retryCount': 0,
        'user.id': req?.user?._id,
        'user.phone.code': req?.user?.phone?.code,
        'user.phone.number': req?.user?.phone?.number,
        'invoice.estimation.serviceTax': tax,
        'invoice.estimation.deliveryCharge': deliveryCharge,
        'invoice.estimation.packaging': packaging,
        'invoice.estimation.products': totalProductAmount,
        'invoice.estimation.nightFare': nightFare,
        'invoice.estimation.total':
            totalProductAmount + packaging + nightFare + deliveryCharge + tax,
        'city.id': city?._id,
        'city.orderCalculation': city?.orderCalculation,
        'city.algorithm': city?.algorithm,
        'shop.id': req?.body?.shopId,
        'activity.initiated.time': new Date(),
        'activity.initiated.location.lat': req?.body?.deliveryAddress?.lat,
        'activity.initiated.location.lng': req?.body?.deliveryAddress?.lng,
        'zone.id': zone?._id ?? undefined,
        'partner.requestReceived': partners.map((each) => ({
            id: each?._id,
            'location.lat': each?.location?.coordinates[1],
            'location.lng': each?.location?.coordinates[0],
            distance:
                parseFloat(
                    turf.distance(
                        {
                            type: 'Point',
                            coordinates: [
                                req?.body?.deliveryAddress?.lng,
                                req?.body?.deliveryAddress?.lat,
                            ],
                        },
                        each?.location
                    )
                )?.toFixed(2) + ' KM',
            time: new Date(),
            status: constantUtils.SENT,
        })),
        'product.orderedProducts': products,
        'payment.option': constantUtils.CASH,
        'payment.paid.status': constantUtils.UNPAID,
        'address.deliveryAddress': req?.body?.deliveryAddress,
        'address.deliveryAddress.type': 'Point',
        'address.deliveryLocation.coordinates': [
            req?.body?.deliveryAddress?.lng,
            req?.body?.deliveryAddress?.lat,
        ],
    });

    order = await Order.findById(order?._id).populate('user.id');

    // Send Notifications
    helper.sendNotifications(
        req,
        constantUtils.PARTNER,
        constantUtils.SOCKET_EVENTS.NEW_REQUEST,
        structureUtils.socketOrder(order),
        partners,
        '',
        'A NEW ORDER'
    );
    return res.json(helper.orderStructure(order));
});

controller.retryRequestOrder = handler(async (req, res) => {
    const order = await Order.findById(req?.body?.orderId)
        .populate('user.id')
        .populate('zone.id')
        .lean();
    if (!order || order?.order?.status !== constantUtils.INITIATED)
        throw '400|' + constantUtils.INVALID_ID;

    let deniedPartners = order?.partner?.requestReceived?.filter(
        (each) => each?.status === constantUtils.DENIED
    );
    const deniedPartnersIDS = deniedPartners?.map((each) => each?.id);

    // Zone Based
    let partners = [];
    let partnerCondition = {};
    partnerCondition['$and'] = [];
    const singleCondition = {
        _id: { $nin: deniedPartnersIDS },
        onlineStatus: true,
        partnerType: constantUtils.STOCK_PARTNER,
        status: constantUtils.ACTIVE,
    };
    if (order?.city?.algorithm?.maxOrdersAtTime) {
        singleCondition[
            `onGoingOrders.${order?.city?.algorithm?.maxOrdersAtTime - 1}`
        ] = {
            $exists: false,
        };
    }
    partnerCondition['$and'].push(singleCondition);
    order?.product?.orderedProducts?.forEach((each) => {
        partnerCondition['$and'].push({
            availableProducts: {
                $elemMatch: {
                    productId: each?._id,
                    status: 'ACTIVE',
                    stockQuantity:
                        each?.productType === constantUtils.QUANTITY_STOCK
                            ? {
                                  $gte: each.quantity,
                              }
                            : undefined,
                },
            },
        });
    });
    if (order?.zone?.id?._id) {
        partnerCondition['$and'].push({
            'zones.zoneId': order?.zone?.id?._id,
            location: {
                $geoWithin: {
                    $geometry: order?.zone?.id?.location,
                },
            },
        });
        partners = await Partner.find(partnerCondition);
        if (partners.length === 0)
            throw '400|' + constantUtils.NO_PARTNERS_AVAILABLE;
    } else {
        partners = await Partner.aggregate([
            {
                $geoNear: {
                    near: {
                        type: 'Point',
                        coordinates: [
                            order?.address?.deliveryAddress?.lng,
                            order?.address?.deliveryAddress?.lat,
                        ],
                    },
                    distanceField: 'dist.calculated',
                    maxDistance:
                        order?.city?.algorithm?.region?.nearByRadius ?? 0,
                    spherical: true,
                },
            },
            { $match: partnerCondition },
        ]);
        if (partners.length === 0)
            throw '400|' + constantUtils.NO_PARTNERS_AVAILABLE;
    }
    helper.sendNotifications(
        req,
        constantUtils.PARTNER,
        constantUtils.SOCKET_EVENTS.NEW_REQUEST,
        structureUtils.socketOrder(order),
        partners,
        '',
        'A NEW ORDER'
    );
    let requestReceivedPartners = [];
    if (deniedPartners?.length > 0)
        deniedPartners?.forEach((each) => {
            requestReceivedPartners.push({
                id: each?.id,
                location: each?.location,
                distance: each?.distance,
                time: each?.time,
                status: each?.status,
            });
        });
    partners.forEach((each) => {
        requestReceivedPartners.push({
            id: each?._id,
            'location.lat': each?.location?.coordinates[1],
            'location.lng': each?.location?.coordinates[0],
            distance:
                parseFloat(
                    turf.distance(
                        {
                            type: 'Point',
                            coordinates: [
                                order?.address?.deliveryAddress?.lng,
                                order?.address?.deliveryAddress?.lat,
                            ],
                        },
                        each?.location
                    )
                )?.toFixed(2) + ' KM',
            time: new Date(),
            status: constantUtils.SENT,
        });
    });
    await Order.findByIdAndUpdate(
        req?.body?.orderId,
        {
            $set: {
                'order.retryCount': (order?.order?.retryCount ?? 0) + 1,
                'partner.requestReceived': requestReceivedPartners,
            },
        },
        {
            new: true,
        }
    );
    return res.json(helper.orderStructure(order));
});

controller.denyOrder = handler(async (req, res) => {
    const order = await Order.findOneAndUpdate(
        {
            _id: req?.body?.orderId,
            'user.id': req?.user?._id,
            'order.status': constantUtils.INITIATED,
        },
        {
            $set: {
                'activity.userDenied.time': new Date(),
                'invoice.final.serviceTax': 0,
                'invoice.final.total': 0,
                'invoice.final.deliveryCharge': 0,
                'invoice.final.nightFare': 0,
                'invoice.final.packaging': 0,
                'invoice.final.products': 0,
                'order.status': constantUtils.USER_DENIED,
            },
        },
        {
            new: true,
        }
    );
    if (!order) throw '400|' + constantUtils.INVALID_ID;
    return res.json(helper.orderStructure(order));
});

controller.expireOrder = handler(async (req, res) => {
    const order = await Order.findOneAndUpdate(
        {
            _id: req?.body?.orderId,
            'order.status': constantUtils.INITIATED,
            'user.id': req?.user?._id,
        },
        {
            $set: {
                'order.status': constantUtils.EXPIRED,
                'invoice.final.serviceTax': 0,
                'invoice.final.total': 0,
                'invoice.final.deliveryCharge': 0,
                'invoice.final.nightFare': 0,
                'invoice.final.packaging': 0,
                'invoice.final.products': 0,
                'activity.expired.time': new Date(),
            },
        },
        {
            new: true,
        }
    );
    if (!order) throw '400|' + constantUtils.INVALID_ID;
    return res.json(helper.orderStructure(order));
});

controller.cancelOrder = handler(async (req, res) => {
    let order = await Order.findOne({
        _id: req?.body?.orderId,
        'user.id': req?.user?._id,
        'order.status': {
            $in: [
                constantUtils.ACCEPTED,
                // constantUtils.DESTINATION_ARRIVED,
                constantUtils.STARTED,
            ],
        },
    }).populate('partner.id');
    if (!order) throw '400|' + constantUtils.INVALID_ID;

    // CANCELLATION CHECK
    if (
        order?.city?.orderCalculation?.user?.cancellation?.status ===
            undefined ||
        order?.city?.orderCalculation?.user?.cancellation?.status === false
    )
        throw '400|' + constantUtils.CANCELLATION_NOT_AVAILABLE;

    // CANCELLATION THRESHOLD CHECK
    if (
        order?.city?.orderCalculation?.user?.cancellation?.threshold &&
        -differenceInMinutes(
            new Date(order?.activity?.accepted?.time),
            new Date()
        ) <=
            order?.city?.orderCalculation?.user?.cancellation?.threshold ===
            false
    )
        throw '400|' + constantUtils.CANCELLATION_TIME_EXCEEDS;

    const updateData = {};

    const updatedProducts = order?.partner?.id?.availableProducts?.map(
        (each) => {
            const orderedProduct = order?.product?.orderedProducts?.find(
                (eachProduct) =>
                    eachProduct?._id?.toString() === each?.productId?.toString()
            );
            if (orderedProduct && each?.stockQuantity) {
                each['stockQuantity'] =
                    each['stockQuantity'] + orderedProduct.quantity;
            }
            return each;
        }
    );

    // return res.json(updatedProducts);

    updateData['order.status'] = constantUtils.USER_CANCELLED;
    updateData['activity.userCancelled.time'] = new Date();
    updateData['activity.userCancelled.location.lat'] = new Date();
    updateData['activity.userCancelled.location.lng'] = new Date();
    updateData['invoice.final.serviceTax'] = 0;
    updateData['invoice.final.deliveryCharge'] = 0;
    updateData['invoice.final.packaging'] = 0;
    updateData['invoice.final.products'] = 0;

    if (
        order?.city?.orderCalculation?.user?.cancellation?.feeStatus &&
        -differenceInMinutes(
            new Date(order?.activity?.accepted?.time),
            new Date()
        ) > order?.city?.orderCalculation?.user?.cancellation?.feeAfterMinutes
    ) {
        updateData['invoice.final.cancellationCharge'] =
            order?.city?.orderCalculation?.user?.cancellation?.feeAmount;
        updateData['invoice.final.total'] =
            order?.city?.orderCalculation?.user?.cancellation?.feeAmount;
    } else {
        updateData['invoice.final.cancellationCharge'] = 0;
        updateData['invoice.final.total'] = 0;
    }

    order = await Order.findByIdAndUpdate(
        req?.body?.orderId,
        {
            $set: updateData,
        },
        {
            new: true,
        }
    )
        .populate('user.id')
        .populate('partner.id');

    await Partner.updateOne(
        {
            _id: order?.partner?.id?._id,
        },
        {
            $set: {
                availableProducts: updatedProducts,
            },
            $pull: {
                onGoingOrders: {
                    orderId: order?._id,
                },
            },
        },
        {
            new: true,
        }
    );

    // Notifications
    // if (order?.user?.id?._id)
    //     helper.sendNotifications(
    //         req,
    //         constantUtils.USER,
    //         constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
    //         structureUtils.socketOrder(order),
    //         [order?.user?.id],
    //         '',
    //         'PARTNER CANCELLED ORDER'
    //     );

    // NOTIFICATIONS
    if (order?.partner?.id?._id)
        helper.sendNotifications(
            req,
            constantUtils.PARTNER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.partner?.id],
            '',
            'USER CANCELLED ORDER'
        );

    helper.sendProductUpdatePartner(order?.partner?.id, req?.app?.io);

    return res.json(structureUtils.orderStructure(order));
});

controller.getRecentOrders = handler(async (req, res) => {
    const condition = {
        'user.id': req?.user?.id,
        'order.status': {
            $in: [
                constantUtils.ACCEPTED,
                constantUtils.STARTED,
                constantUtils.DESTINATION_ARRIVED,
                constantUtils.DELIVERED,
            ],
        },
    };
    const count = await Order.count(condition);
    const orders = await Order.find(condition)
        .select('order invoice product payment address')
        .sort('-_id')
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    // Activity.create({
    //     fromId: req.user._id,
    //     fromType: constantUtils.ACTIVITIES.ADMINS,
    //     action: constantUtils.ACTIVITIES.VIEW_LIST,
    //     listType: constantUtils.USERS,
    // });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: orders.map((each) => ({
            id: each?._id,
            orderId: each?.order?.uniqueId,
            type: each?.order?.orderType,
            status: each?.order?.status,
            invoice: each?.invoice.estimation,
            products: each?.product?.orderedProducts?.map((each) =>
                structureUtils.productStructure(each)
            ),
            paymentType: each?.payment?.option,
            address: each?.address?.deliveryAddress,
        })),
    });
});

controller.getOrderDetail = handler(async (req, res) => {
    const order = await Order.findById(req?.body?.orderId)
        .populate('user.id')
        .populate('partner.id');
    if (!order) throw '400|' + constantUtils.INVALID_ID;
    return res.json(structureUtils.orderStructure(order));
});

controller.getOrdersList = handler(async (req, res) => {
    let condition = {
        'user.id': req?.user?._id,
    };

    // Sort
    let sort = '-_id';

    // Filters
    if (req?.body?.orderType === 'ALL')
        condition['order.status'] = {
            $nin: [constantUtils.INITIATED, constantUtils.EXPIRED],
        };

    if (req?.body?.orderType === 'COMPLETED')
        condition['order.status'] = constantUtils.DELIVERED;

    if (req?.body?.orderType === 'ONGOING')
        condition['order.status'] = {
            $in: [
                constantUtils.ACCEPTED,
                constantUtils.STARTED,
                constantUtils.DESTINATION_ARRIVED,
            ],
        };

    console.log(condition);

    const count = await Order.count(condition);
    const orders = await Order.find(condition)
        .select('-activity')
        .populate('partner.id')
        .sort(sort)
        .skip(helper.getSkip(req))
        .limit(helper.getLimit(req))
        .lean();

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: orders?.map((each) => structureUtils.orderStructure(each)),
    });
});

controller.trackPartnerLocation = handler(async (req, res) => {
    const partner = await Partner.findById(req?.body?.partnerId).select(
        'location'
    );
    if (!partner) throw '400|' + constantUtils.INVALID_ID;
    return res.json({
        lat: partner?.location?.coordinates[1] ?? 0,
        lng: partner?.location?.coordinates[0] ?? 0,
    });
});

controller.socketTrackPartnerLocation = async (io, socket, user, body) => {
    const partner = await Partner.findById(body?.partnerId).select('location');
    if (partner)
        io.of(constantUtils.NS.USERS)
            .to(socket?.id)
            ?.emit(constantUtils?.SOCKET_EVENTS?.TRACK_PARTNER_LOCATION, {
                lat: partner?.location?.coordinates[1] ?? 0,
                lng: partner?.location?.coordinates[0] ?? 0,
            });
};

// partners: partners?.map((each) => ({
//     id: each?.id,
//     firstName: each?.firstName,
//     lastName: each?.lastName,
//     email: each?.email,
//     avatar: each?.avatar,
//     distance: parseFloat(each?.distance / 1000).toFixed(2) + ' KM',
// })),
// Getting Partners
// const zone = await Zone.findOne({
//     location: {
//         $geoIntersects: {
//             $geometry: {
//                 type: 'Point',
//                 coordinates: [
//                     req?.body?.deliveryAddress?.lng,
//                     req?.body?.deliveryAddress?.lat,
//                 ],
//             },
//         },
//     },
//     status: constantUtils.ACTIVE,
// }).select('zoneName');
// zone: {
//     insideZone: zone ? true : false,
//     zoneName: zone?.zoneName ?? undefined,
//     zoneId: zone?._id,
// },
// if (zone) {
//     partners = await Partner.count({
//         'zones.zoneId': zone?._id,
//         partnerType: constantUtils.STOCK_PARTNER,
//         status: constantUtils.ACTIVE,
//         onlineStatus: true,
//     });
// } else if (city?.algorithm?.region?.nearByRadius) {
//     partners = await Partner.aggregate([
//         {
//             $geoNear: {
//                 near: {
//                     type: 'Point',
//                     coordinates: [
//                         req?.body?.deliveryAddress?.lng,
//                         req?.body?.deliveryAddress?.lat,
//                     ],
//                 },
//                 distanceField: 'dist.calculated',
//                 maxDistance: city?.algorithm?.region?.nearByRadius,
//                 spherical: true,
//             },
//         },
//         {
//             $match: {
//                 partnerType: constantUtils.STOCK_PARTNER,
//                 status: constantUtils.ACTIVE,
//                 onlineStatus: true,
//             },
//         },
//         { $count: 'firstName' },
//     ]);
// }

module.exports = controller;

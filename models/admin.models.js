const mongoose = require('mongoose');

const redis = require('../utils/redis.utils');
const constantUtils = require('../utils/constant.utils');

const adminSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            trim: true,
            enum: [constantUtils.OPERATORS, constantUtils.DEVELOPER],
            index: true,
        },
        data: {
            firstName: {
                type: String,
                trim: true,
                optional: true,
                index: true,
            },
            lastName: {
                type: String,
                trim: true,
                optional: true,
                index: true,
            },
            gender: {
                type: String,
                enum: [
                    constantUtils.MALE,
                    constantUtils.FEMALE,
                    constantUtils.OTHERS,
                ],
                trim: true,
                optional: true,
            },
            expoToken: {
                type: String,
            },
            phone: {
                code: {
                    type: true,
                    type: String,
                    optional: true,
                    trim: true,
                    index: true,
                },
                number: {
                    unique: true,
                    type: String,
                    trim: true,
                    optional: true,
                    index: {
                        unique: true,
                        partialFilterExpression: {
                            number: { $type: 'string' },
                        },
                    },
                },
            },
            socketId: {
                type: String,
                index: true,
            },
            extraPrivileges: {
                type: Object,
                optional: true,
            },
            status: {
                type: String,
                optional: true,
                trim: true,
                enum: [
                    constantUtils.ACTIVE,
                    constantUtils.INACTIVE,
                    constantUtils.ARCHIEVE,
                ],
                default: constantUtils.ACTIVE,
            },
            avatar: {
                type: String,
                optional: true,
            },
            accessToken: {
                type: String,
                optional: true,
                index: true,
            },
            languageCode: {
                type: String,
                optional: true,
            },
            lastEdited: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'admins',
            },
            addedBy: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'admins',
            },
            password: {
                type: String,
                optional: true,
            },
            lastEditedTime: {
                type: Date,
                optional: true,
            },
            lastLogin: {
                type: Date,
                optional: true,
            },
            loginCount: {
                type: Number,
                optional: true,
            },
            email: {
                type: String,
                unique: true,
                optional: true,
                trim: true,
                index: {
                    unique: true,
                    partialFilterExpression: {
                        email: { $type: 'string' },
                    },
                },
            },
            preferredTheme: { type: String, optional: true },
            forgotPasswordOtp: { type: String, optional: true },
        },
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

const Admin = mongoose.model('admins', adminSchema);

module.exports = Admin;

/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtil = require('../utils/constant.utils');
const redis = require('../utils/redis.utils');

const PointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
    },
    coordinates: {
        type: [Number],
    },
});

const AvailableProductSchema = new mongoose.Schema({
    productId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'products',
        index: true,
    },
    productType: {
        type: String,
        enum: [
            constantUtil.PREPARATION,
            constantUtil.QUANTITY_STOCK,
            constantUtil.STATUS_STOCK,
        ],
        index: true,
    },
    status: {
        type: String,
        required: true,
    },
    maxStockQuantity: {
        type: Number,
        min: 0,
    },
    lastReFillTime: {
        type: Date,
    },
    stockQuantity: {
        type: Number,
        min: 0,
    },
    preparationTime: {
        type: Number,
        min: 0,
    },
});

const shopSchema = new mongoose.Schema(
    {
        cityId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'cities',
        },
        shopType: {
            type: String,
            enum: [constantUtil.RESTAURANTS, constantUtil.HEADS],
            default: constantUtil.RESTAURANTS,
            requried: true,
            index: true,
        },
        headId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'shops',
            index: true,
        },
        zoneId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'zones',
        },
        shopName: {
            type: String,
            trim: true,
            index: true,
        },
        email: {
            type: String,
            trim: true,
            lowercase: true,
            index: {
                unique: true,
                partialFilterExpression: { email: { $type: 'string' } },
            },
        },
        phone: {
            code: {
                type: String,
                required: true,
                trim: true,
            },
            number: {
                type: String,
                unique: true,
                required: true,
                trim: true,
            },
        },
        otp: {
            type: String,
            trim: true,
        },
        serviceAvailableTime: {
            days: [
                {
                    type: String,
                },
            ],
            startTime: {
                type: Date,
            },
            endTime: {
                type: Date,
            },
        },
        onlineStatus: {
            type: Boolean,
            required: true,
        },
        phoneNumberOtp: {
            optional: true,
            type: String,
            trim: true,
        },
        phoneNumberVerifiedTime: {
            optional: true,
            type: Date,
        },
        avatar: {
            type: String,
            default: constantUtil.DEFAULTAVATAR,
            trim: true,
        },
        languageCode: {
            type: String,
            default: 'en',
            trim: true,
        },
        password: {
            type: String,
            required: true,
        },
        emailToken: {
            type: String,
            optional: true,
            // index: {
            //     unique: true,
            //     partialFilterExpression: {
            //         emailToken: { $type: 'string' },
            //     },
            // },
        },
        accessToken: {
            type: String,
            optional: true,
            index: true,
        },
        lastLogin: {
            type: Date,
            optional: true,
        },
        loginCount: {
            type: Number,
            optional: true,
        },
        emailVerificationTime: {
            optional: true,
            type: Date,
        },
        address: {
            placeId: {
                type: String,
            },
            addressName: {
                type: String,
                lowercase: true,
            },
            fullAddress: {
                type: String,
            },
            shortAddress: {
                type: String,
            },
            lat: {
                type: Number,
            },
            lng: {
                type: Number,
            },
        },
        status: {
            /* INCOMPLETE, ACTIVE, INACTIVE, ARCHIVE */
            type: String,
            required: true,
            trim: true,
            enum: [
                constantUtil.INCOMPLETE,
                constantUtil.ACTIVE,
                constantUtil.INACTIVE,
                constantUtil.ARCHIEVE,
            ],
            default: constantUtil.ACTIVE,
            index: true,
        },
        statusLastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        password: {
            type: String,
            optional: true,
        },
        lastEditedTime: {
            type: Date,
            optional: true,
        },
        lastLogin: {
            type: Date,
            optional: true,
        },
        loginCount: {
            type: Number,
            optional: true,
        },
        email: {
            type: String,
            unique: true,
            optional: true,
            trim: true,
            index: {
                unique: true,
                partialFilterExpression: {
                    email: { $type: 'string' },
                },
            },
        },
        availableProducts: [AvailableProductSchema],
        preferredTheme: { type: String, optional: true },
        forgotPasswordOtp: { type: String, optional: true },
        location: {
            _id: 0,
            type: PointSchema,
            index: '2dsphere',
            default: {
                type: 'Point',
                coordinates: [0, 0],
            },
        },
        catalogueId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'catalogues',
        },
    },
    { timestamps: true, versionKey: false }
);

const Shop = mongoose.model('shops', shopSchema, 'dshops');

Shop.AvailableProductSchema = AvailableProductSchema;

Shop.getShop = async function (id) {
    const ifShop = await redis.get(id);
    if (ifShop) return ifShop;
    const shop = await Shop.findById(id);
    await redis.set(id, shop);
    return shop;
};

Shop.refreshRedis = async function (id) {
    const shop = await Shop.findById(id);
    await redis.set(id, shop);
};

module.exports = Shop;

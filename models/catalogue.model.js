/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtil = require('../utils/constant.utils');

const translationSchema = new mongoose.Schema({
    languageCode: {
        type: String,
        trim: true,
    },
    languageId: {
        type: mongoose.Schema.Types.ObjectId,
        optional: true,
        ref: 'languages',
    },
    title: {
        type: String,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
});

const catalogueSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            index: true,
        },
        headId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'shops',
            index: true,
        },
        // cities: [
        //     {
        //         cityId: {
        //             type: mongoose.Schema.Types.ObjectId,
        //             required: true,
        //             ref: 'cities',
        //             index: true,
        //         },
        //     },
        // ],
        owners: [
            {
                type: mongoose.Schema.Types.ObjectId,
                required: true,
                ref: 'shops',
            },
        ],
        categories: [
            {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'categories',
            },
        ],
        shopId: [
            {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'shops',
            },
        ],
        images: [String],
        status: {
            type: String,
            required: true,
            trim: true,
            enum: [constantUtil.ACTIVE, constantUtil.INACTIVE],
            default: constantUtil.ACTIVE,
            index: true,
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        lastEditedTime: {
            type: Date,
            optional: true,
        },
    },
    { timestamps: true, versionKey: false }
);

const catalogues = mongoose.model('catalogues', catalogueSchema, 'dcatalogues');

module.exports = catalogues;
